(local subtile (require :lib.subtile))

(local map {})

;; this should be wrapped into a library with subtile and
;; some of the logic from prefab-map
(fn map.update-tileset-batch [batch mapin layer]
  (: batch :clear)
  (local level (. mapin :data layer));;(or layers? [:sun :clouds :ground :objs]))
  ;; subtiles are half the size of regular tiles
  (each [id tile (pairs (. mapin :data layer))]
    ;; (pp (. mapin :tile-set tile.type))
    (subtile.batch batch (/ mapin.tile-size 2)  tile.x  tile.y
                   (. mapin :tile-set tile.type :quad) tile.index))
  (: batch :flush))

(fn map.xy-to-tile [[x y] tile-size]
  [(math.floor (/ (- x 0) (* 1 tile-size)))
   (math.floor (/ (- y 0) (* 1 tile-size)))])

(fn xy-to-id [x y width]
    (+ (* x width) y))

(fn id-to-xy [id width]
    [(math.floor (/ id width)) (math.floor (% id width))])

(fn map.create-quads [sample-grid tile-types]
  (each [key map (pairs tile-types)]
    (tset map :quad ((. subtile map.size) sample-grid (unpack map.pos))))
  tile-types)

(fn map.clear-quads [tile-types]
  (each [key map (pairs tile-types)]
    (tset map :quad nil))
  tile-types)

(fn map.to-sparse [mapin layer]
  (local {: type : map : data} (. mapin :data layer))
  (local sparse {})
  (when (= type "dense")
    (local off-y (or (. mapin :data layer :off-y) 0))
    (local range (or (. mapin :data layer :range)
                     {:minx 1 :miny 1 :maxx mapin.width :maxy mapin.height}))
    (for [i range.minx range.maxx]
      (for [j range.miny range.maxy]
        (local type (. map (. data i (- j off-y))))
        (tset sparse  (+ (* i mapin.width) j )
              {:x i :y j  : type})
         ))
    (tset (. mapin :data) layer sparse))
  mapin)

(fn map.to-dense [mapin layer default]
  (fn xy-to-index [x y] (+ (* x mapin.width) y))
  (local dense {:type "dense"})
  (local sparse (. mapin :data layer))
  ;; generate map
  (local type-map {})
  (var i 0)
  (var minx 180)
  (var maxx 1)
  (var miny 180)
  (var maxy 1)
  (each [key value (pairs sparse)]
    (when (~= value.type :chasm)
      (when (> value.x maxx) (set maxx value.x))
      (when (< value.x minx) (set minx value.x))
      (when (> value.y maxy) (set maxy value.y))
      (when (< value.y miny) (set miny value.y)))
    (when (= nil (. type-map value.type))
      (tset type-map value.type i)
      (set i (+ i 1))))
  ;; create a reversed map (temp)
  (local map-rev {})
  (each [key value (pairs type-map)]
    (tset map-rev value key))
  ;; sparse -> dense data
  (local data [])
  (set minx (- minx 1))
  (set maxx (+ maxx 1))
  (set miny (- miny 1))
  (set maxy (+ maxy 1))
  (for [i minx maxx]
    (tset data i [])
    (for [j miny maxy]
      (local value (or (. sparse (xy-to-index i j)) {:type (or default :chasm)}))
      (tset (. data i) (+ 1 (- j miny)) (. type-map value.type))))
  (tset dense :off-y (- miny 1))
  (tset dense :data data)
  (tset dense :map map-rev)
  (tset dense :range {: minx : maxx : miny : maxy})
  (tset (. mapin :data) layer dense)
  mapin)

(local
 tile-set
 {:chasm {:layer :ground :pos [1 1] :size :square10 :auto :blob :collidable :fall}
  :ground {:layer :ground :pos [6 1] :size :square4 :auto :fixed :collidable :none}})

(fn map.new [w h gsx gex gsy gey]
  (fn zeros [value number1 number2]
    (var ret [])
    (for [i 1 number1]
      (var sub [])
      (for [j 1 number2] (table.insert sub value))
      (table.insert ret sub))
    ret)
  (local mapin {:data {:ground {:type "dense" :map {0 :ground 1 :chasm}
                                :data (zeros 1 w h)}}

                :tile-set
                tile-set
                :width w :height h :id 0 :tile-size 8})

    (for [i gsx gex]
      (for [j gsy gey]
        (tset (. mapin.data.ground.data i) j  0)
        ))
    (map.load mapin)
  ;;(local quad (. mapin.tile-set :ground :quad))
  ;; (map.auto-index mapin :ground)
  mapin)

(fn map.load [mapin]
  (local sample-grid (subtile.newGrid 4 4 256 256))
  (map.create-quads sample-grid mapin.tile-set)
  (map.to-sparse mapin :ground)
  (map.to-dense mapin :ground)
  (map.to-sparse mapin :ground)
  (map.auto-index mapin :ground)
  mapin)

(fn map.add-tile [mapin x y l tile ?details]
    (var index (xy-to-id x y mapin.width))
    (when tile
      (tset tile :x x)
      (tset tile :y y)
      (tset tile :l l)
      (tset tile :id mapin.id)
      (tset map :id (+ mapin.id 1))
      (when ?details
        (each [key value (pairs ?details)]
          (tset tile key value))))
    (local replace (. mapin.data l index))
    (tset mapin.data l index tile)
    (values tile replace))

(fn map.remove [mapin x y l]
  (var index (xy-to-id x y mapin.width))
  (var tile nil)
  (when (. mapin.data l index)
    (set tile (lume.clone (. mapin.data l index))))
  (tset mapin.data l index nil)
    (values tile nil))

(fn map.replace [mapin x y l tile ?details]
  (map.remove mapin x y l)
  (map.add-tile mapin x y l tile ?details))

(fn neighbour [tiles width x y i j]
  (var max 0)
  (let [xp (+ x i)
        yp (+ y j)
        tile (. tiles (xy-to-id xp yp width))]
    (if tile
        (if tile.type
            (values tile.type :ok)
            (values :tile-missing-type :error ))
        (values :chasm :error))))

(fn get-neighbours [tiles x y width]
    (let [neighbour? (fn [i j] (neighbour tiles width x y i j))]
      {:right (neighbour? 1 0)
       :left (neighbour? -1 0)
       :up (neighbour? 0 -1)
       :down (neighbour? 0 1)
       :up-right (neighbour? 1 -1)
       :up-left (neighbour? -1 -1)
       :down-right (neighbour? 1 1)
       :down-left (neighbour? -1 1)}))

(fn neighbour-to-tile [x y direction]
  (let [neighbour? (fn [i j] {:x (+ x i) :y (+ y j)})]
    (match direction
      :right (neighbour? 1 0)
      :left (neighbour? -1 0)
      :up (neighbour? 0 -1)
      :down (neighbour? 0 1)
      :up-right (neighbour? 1 -1)
      :up-left (neighbour? -1 -1)
      :down-right (neighbour? 1 1)
      :down-left (neighbour? -1 1))
    ))

(fn map.auto-index [mapin layer]
  (local width mapin.width)
  (local tile-set mapin.tile-set)
  (local tiles (. mapin.data layer))
  (each [key tile (pairs tiles)]
    (local [type x y] [tile.type tile.x tile.y])
    (tset (. mapin.data layer key) :index
          ((. subtile (. tile-set type :auto)) type (get-neighbours tiles x y width))))
  mapin)

(fn map.newGrid [...]
  (subtile.newGrid ...))

map
