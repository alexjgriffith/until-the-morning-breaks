#!/bin/bash

VERSION=0.2.12

make love
mv releases/* tools/love.js
make clean
cd tools/love.js
love-js until-the-morning-breaks-0.2.11.love change -t "Sample" -c
cp index.html change/index.html
cp love.css change/theme/love.css
cp consolewrapper.js change/consolewrapper.js
zip -r  -q change change/
mv change.zip until-the-morning-breaks-web-$VERSION.zip
butler push --userversion $VERSION until-the-morning-breaks-web-$VERSION.zip alexjgriffith/until-the-morning-breaks:web
scp -r change/ alexjgriffith:~/nginx/resources/vid/utmb/
cd ../../
