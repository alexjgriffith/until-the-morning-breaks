love.conf = function(t)
   t.gammacorrect = true
   t.title, t.identity = "until-the-morning-breaks", "until-the-morning-breaks"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 1280
   t.window.height = 720--540
   t.window.vsync = 1
    t.window.minwidth = 1280
    t.window.minheight = 720
    t.window.resizable = true
   t.window.centered = true
   t.window.borderless= false
   t.window.fullscreentype = "desktop" -- exclusive
   t.window.fullscreen = false
   t.version = "11.3"
   t.gammacorrect = true
end
