VERSION=0.2.12
LOVE_VERSION=11.3
NAME=until-the-morning-breaks
ITCH_ACCOUNT=alexjgriffith
URL=https://gitlab.com/alexjgriffith/until-the-morning-breaks
AUTHOR="Alexander Griffith"
DESCRIPTION="Don't let the fire go out."


LIBS_LUA := $(wildcard lib/*)
LIBS_FNL := $(wildcard lib/*.fnl)
LUA := $(wildcard *.lua)
SRC := $(wildcard src/*.fnl)
OUT := $(patsubst src/%.fnl,%.lua,$(SRC))
OUT_LIBS := $(patsubst %.fnl,%.lua,$(LIBS_FNL))

run: $(OUT) $(OUT_ENTS); love .

count: ; cloc src/*.fnl --force-lang=clojure

clean: ; rm -rf releases/* $(OUT) $(OUT_LIBS)

cleansrc: ; rm -rf $(OUT) $(OUT_LIBS)

%.lua: src/%.fnl; lua lib/fennel --compile --correlate $< > $@

lib/%.lua: lib/%.fnl; lua lib/fennel --compile --correlate $< > $@

LOVEFILE=releases/$(NAME)-$(VERSION).love

$(LOVEFILE): $(LUA) $(OUT) $(OUT_LIBS) $(LIBS_LUA) assets #text
	mkdir -p releases/
	find $^ -type f | LC_ALL=C sort | env TZ=UTC zip -r -q -9 -X $@ -@

love: $(LOVEFILE)

# platform-specific distributables

REL=$(PWD)/love-release.sh # https://p.hagelb.org/love-release.sh
FLAGS=-a "$(AUTHOR)" --description $(DESCRIPTION) \
	--love $(LOVE_VERSION) --url $(URL) --version $(VERSION) --lovefile $(LOVEFILE)

releases/$(NAME)-$(VERSION)-x86_64.AppImage: $(LOVEFILE)
	cd appimage && ./build.sh $(LOVE_VERSION) $(PWD)/$(LOVEFILE)
	mv appimage/game-x86_64.AppImage $@

releases/$(NAME)-$(VERSION)-macos.zip: $(LOVEFILE)
	$(REL) $(FLAGS) -M
	mv releases/$(NAME)-macos.zip $@

releases/$(NAME)-$(VERSION)-win.zip: $(LOVEFILE)
	$(REL) $(FLAGS) -W32
	mv releases/$(NAME)-win32.zip $@

linux: releases/$(NAME)-$(VERSION)-x86_64.AppImage
mac: releases/$(NAME)-$(VERSION)-macos.zip
windows: releases/$(NAME)-$(VERSION)-win.zip

# If you release on itch.io, you should install butler:
# https://itch.io/docs/butler/installing.html

uploadlinux: releases/$(NAME)-$(VERSION)-x86_64.AppImage
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):linux --userversion $(VERSION)
uploadmac: releases/$(NAME)-$(VERSION)-macos.zip
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):mac --userversion $(VERSION)
uploadwindows: releases/$(NAME)-$(VERSION)-win.zip
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):windows --userversion $(VERSION)
uploadlove: releases/$(NAME)-$(VERSION).love
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):love --userversion $(VERSION)

upload: uploadlinux uploadwindows uploadlove uploadmac

release: linux mac windows upload cleansrc
