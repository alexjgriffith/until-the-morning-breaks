(local footsteps {})

;;id = SpriteBatch:add( x, y, r, sx, sy, ox, oy, kx, ky )
;; id = SpriteBatch:add( quad, x, y, r, sx, sy, ox, oy, kx, ky )
(fn add-step [self x y angle]
  (tset self.prints self.index {: x : y : angle :foot self.foot})
  (if (= self.foot :left)
      (tset self :foot :right)
      (tset self :foot :left))
  (tset self :index (+ self.index 1))
  (when (> self.index self.max)
    (tset self :index 1))
  (self.spritebatch:clear)
  (let [quad {:left self.quad-left :right self.quad-right}]
    (each [_ {: x : y : angle : foot} (ipairs self.prints)]
      (self.spritebatch:add (. quad foot) x y angle 1 1 4 4)))
  (self.spritebatch:flush))

(fn footsteps.update [self {: x : y} angle]
  (let [distance (lume.distance self.last-footprint.x self.last-footprint.y x y)]
    (when (> distance self.stride)
        (do (add-step self x y angle) ;; this could be done more accuratly
            (tset self :last-footprint {: x : y})))))

(fn footsteps.draw [self]
  (love.graphics.push "all")
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.draw self.spritebatch 0 0)
  (love.graphics.pop)
  )

(local footsteps-mt {:__index footsteps
                     :update footsteps.update
                     :draw footsteps.draw})

(fn footsteps.new [x y image quad-left quad-right stride max?]
  (let [max (or max? 100)
        spritebatch  (love.graphics.newSpriteBatch image (+ max 1) :static)]
  (setmetatable {:last-footprint {:x x :y y}
                 :prints []
                 : quad-left
                 : quad-right
                 : spritebatch
                 : stride
                 : max
                 :foot :left
                 :index 1}
                footsteps-mt)))

footsteps
