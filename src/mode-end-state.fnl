(local end-state {})

(var timer 0)

(local state (require :state))

(var step :leave)

(var alpha 0)

(var alpha-2 0)

(local gamestate (require :lib.gamestate))
(local buttons (require :lib.buttons))
(local params (require :params))
(local mode-game (require :mode-game))

(local play-again (require :play-again))

(local assets _G.assets)

(local credit-text
       "
Game (Art and Code) by Alexander Griffith

Font (Avara) - (OFL)
Font (Inconsolata) - (OFL)
Library (Lume) - RXI (MIT/X11)
Library (anim8,bump,gamera) - Enrique Cota (MIT)
Engine (LÖVE) - LÖVE Dev Team (Zlib)
Language (Fennel) - Calvin Rose (MIT/X11)
Sounds - InspectorJ, JoseAgudelo, Dobroide, Gerainsan (CC0)")

(var enough-animals false)

(var no-animals false)

(var all-animals false)

(fn elements [top-line]
       [{:type :title :y 50 :w 400 :h 60 :text "Until the Morning Breaks"}
        {:type :subtitle :y 125 :w 400 :h 60 :text "( Kindred Game Jam Submission )"}
        {:type :small-text :y 200 :w 400 :h 60 :text top-line}
        {:type :subtitle :y 300 :w 400 :h 60 :text "Credits"}
        {:type :button :y 620 :oy -13 :ox 200 :w 350 :h 70 :text "Play Again"}
        {:type :button :y 620 :oy -13 :ox -200 :w 350 :h 70 :text "Quit"}
        ;; {:type :button :y 620 :oy -13 :ox -425 :w 350 :h 70 :text "Hard Mode"}
        ;; {:type :button :y 620 :oy -13 :ox 425 :w 350 :h 70 :text "Easy Mode"}
        ;; {:type :invisible-button :y 150 :oy -10 :ox 425 :w 350 :h 60 :text "Sandbox Mode"}
        ;; {:type :invisible-button :y 150 :oy -10 :ox -425 :w 350 :h 60 :text "Good Luck"}
        {:type :small-text :y 350 :oy -10 :ox 0 :w 900 :h 70 :text credit-text}])

(local element-font
       {:title  ((. assets.fonts "avara") 70)
        :subtitle  ((. assets.fonts "avara") 40)
        :button  ((. assets.fonts "avara") 40)
        :small-text  ((. assets.fonts "inconsolata") 20)})

(local element-click
       {"Quit"
        (fn []
          (if (not state.web)
              (love.event.quit)
              (JS.callJS "closeFullScreen();")))
        "Play Again"
        play-again.normal
        "Easy Mode"
        play-again.easy
        "Hard Mode"
        play-again.hard
        "Sandbox Mode"
        play-again.sandbox
        "Good Luck"
        play-again.gg
        })

(local element-hover {:button (fn [element x y] :nothing)
                      :invisible-button (fn [element x y] :nothing)})

(var ui (buttons (elements state.top-line) params element-click element-hover element-font))

(local menu-canvas (love.graphics.newCanvas 1280 720))
(fn end-state.draw [self]
  (mode-game.draw self)
  (love.graphics.setCanvas menu-background)
  (love.graphics.clear (/ 34 256) (/ 32 256) (/ 52 256) (lume.clamp alpha 0 1))
  (love.graphics.setCanvas menu-canvas)
  (love.graphics.clear)
  (love.graphics.setColor 1 1 1 1)
  (var saved state.saved)
  (when (= :load step)
    (love.graphics.setFont ((. assets.fonts "avara") 70))
    (match [state.game-over saved]
      [:fireout _]
      (love.graphics.printf ["The fire burnt out\nNow for eternal darkness"] 20 250 1240 :center)
      [:hunter _]
      (love.graphics.printf [(.. "You were devoured by\n" state.hunter)] 20 250 1240 :center)
      [:gems 6]
      (love.graphics.printf ["The curse has been lifted!\n\nA new morning dawns for all the little creatures"]  20 150 1240 :center)
      [:gems _]
      (love.graphics.printf ["The curse has been lifted!\n\nBut the poor creatures are left in the darkness :("]  20 150 1240 :center))
    )

  (love.graphics.setCanvas menu-background)
  (love.graphics.setColor 1 1 1 (lume.clamp alpha-2 0 1))
  (love.graphics.rectangle "fill" 0 0 1920 1080)
  (love.graphics.setCanvas menu-canvas)
  ;; (love.graphics.setColor 1 1 1 1)
  (if (and (~= step :leave ) (~= step :load )
           (or (= state.game-over :hunter)
               (= state.game-over :fireout)))
      (play-again.repeat)

      (and (~= step :leave ) (~= step :load ))
      (do
        (ui:draw)
          (local difficulty-text
                 (. {:default "Normal Mode"
                     :hard "Hard Mode"
                     :easy "Easy Mode"
                     :gg "Good Luck"
                     :sandbox "Sandbox Mode"}
                    state.difficulty))
          (local death-text
                 (. {:gems (.. (or state.saved 0) " little animals saved!")
                     :fireout "Death by Darkness"
                     :hunter (.. "Death by " state.hunter)
                     :skip "Skipped to Credits"
                     }
                    state.game-over))
          (love.graphics.printf (.. difficulty-text ": " death-text)
                                (+ 0 0) (+ 250 0) 1280 :center))
      )
  ;; (when (= :enter step) (ui:draw))
  ;; (when (= :done step) (ui:draw))
  (love.graphics.setCanvas)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.draw menu-background 0 0 0 2)
  (love.graphics.draw menu-canvas offsets.x offsets.y)
  )

(fn end-state.update [self dt]
  (mode-game.update self dt)
  (ui:update dt)
  (match step
    :leave
    (do
      (if (> timer 0.2)
          (do
            (set step :load)
            (set timer 0)
            (set alpha 1))
          (do
            (set timer (+ timer dt))
            (set alpha timer)))
      )
    :load
    (do
      (if (> timer 5)
          (do (set step :enter)
              (set timer 0))
          (set timer (+ timer dt))))
    :enter
    (do
      (love.mouse.setVisible true)
      (if (> timer 1.5)
          (do
            (set step :done)
            (set timer 0)
            (set alpha-2 1))
          (do
            (set timer (+ timer dt))
            (set alpha-2 timer)))
      )
    :done (do
            (love.mouse.setVisible true))
    )
  )

(fn end-state.enter [self from why]
  (love.mouse.setVisible false)
  (set timer 0)
  (set alpha 0)
  (set alpha-2 0)
  (if (= :skip state.game-over)
      (do
        (set alpha 0)
        (set alpha-2 1)
        (set step :done))
      (set step :leave))
  (local objects state.objects)
  (var safe-anims 0)
  (each [_ obj (ipairs objects)]
    (when (and (= obj.type :monster) obj.safe)
      (set safe-anims (+ safe-anims 1))))
  (tset state :saved safe-anims)
  (tset state :top-line "Sample")
  (match [state.game-over safe-anims]
    [:fireout 0]
    (tset state :top-line "You sit alone as the last embers slowly die. The night is eternal.")
    [:fireout _]
    (tset state :top-line "As the final embers finally go out, you watch your animal friends succumb to the darkness.")
    [:hunter _]
    (tset state :top-line "A devoured saviour can save no one")
    [:gems 6]
    (tset state :top-line "The light breaks over the horizon and you walk away with all your new found animal friends.")
    [:gems _]
    (tset state :top-line "You sit alone as the light breaks over the horizon.")
    [:skip _]
    (tset state :top-line "")
    )
  (set ui (buttons (elements state.top-line) params element-click element-hover element-font))
  (tset state :menu true))

(fn end-state.leave [self from ...]
  (tset state :menu false))

(fn end-state.mousereleased [self ...]
  (when (= :done step)
    (ui:mousereleased ...)))

(fn end-state.keypressed [self key code]
  (match key
    :f10 (when (not state.web) (toggle-fullscreen))
    "m" (toggle-sound)
    "q" (when (not state.web) (screenshot))
    "h" (play-again.hard)
    "e" (play-again.easy)
    "n" (play-again.default)
    "g" (play-again.gg)
    "b" (play-again.sandbox)
    "return"  (play-again.same)
    "tab"  (play-again.same)
    "escape" (play-again.same)
    ))

end-state
