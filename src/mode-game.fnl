(local game {})

(local lights (require :lights))

(local state (require :state))

(local params (require :params))

(var has-drawn-ground false)

(local snow (require :snow))

(local debug-steering (require :debug-steering))

(local ground-canvas (love.graphics.newCanvas (* 2 1280) (* 2 720)))
(ground-canvas:setFilter :nearest :nearest)
(local ground-quad (love.graphics.newQuad 0 0 640 360 1280 720))
(local assets _G.assets)

(local ui-font (_G.assets.fonts.avara 30))

(local gif ((require :lib.save-gifs) "example"))
(local dev true)

(fn draw-debug []
  (each [_ obj (ipairs state.objects)]
    (when (or (= obj.type :monster) (= obj.type :hunter))
      (debug-steering obj))))

(fn draw-ground []
  (when true ;;(not G.has-drawn-ground) ;; needs to be reset on full screen
    (love.graphics.push "all")
    (love.graphics.setCanvas ground-canvas)
    (love.graphics.clear)
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.clear)
    (love.graphics.setColor params.colours.background)
    (love.graphics.rectangle "fill" 0 0 (* 2 1280) (* 2 720))
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.draw state.level.tileset-batch)
    (love.graphics.pop)
    (set has-drawn-ground true))
  (love.graphics.push "all")
  (love.graphics.setCanvas draw-canvas)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.clear)
  (ground-quad:setViewport  (+  camera.x  0 0)
                            (+  camera.y  0 0)
                            320 180
                            (* 2 1280)
                            (* 2 720))
  (love.graphics.draw ground-canvas ground-quad  0 0 0 1)
  (love.graphics.pop)
  )

(fn draw-overlay []
  ;; (love.graphics.setFont ((. assets.fonts "avara") 130))
  ;; (love.graphics.printf "Survive" 0 150 1280 :center)
  ;; (love.graphics.setFont ((. assets.fonts "avara") 45))
  ;; (love.graphics.printf "Until the Morning Breaks" 0 550 1280 :center)

  ;; (love.graphics.rectangle "line" 325 110 630 500)
  ;; (when (not state.menu)
  ;;   (love.graphics.setFont ((. assets.fonts "avara") 130))
  ;;   (love.graphics.printf "Survive" 0 50 1280 :center)
  ;;   (love.graphics.setFont ((. assets.fonts "avara") 50))
  ;;   (love.graphics.printf "Until the Morning Breaks" 0 650 1280 :center)
  ;; )

  ;; (love.graphics.setFont ((. assets.fonts "avara") 70))
  ;; ;;(love.graphics.printf "Until the Morning Breaks" 0 650 1280 :center)
  ;; (love.graphics.printf "Until the Morning Breaks" 545 465 600 :right)

  ;; (love.graphics.rectangle "line" 200 100 540 540)
  ;; ;;(love.graphics.printf "Until the Morning Breaks" 0 650 1280 :center)
  ;; (love.graphics.setFont ((. assets.fonts "avara") 55))
  ;; (love.graphics.printf "Play with Snuffles!" 200 150 540 :center)
  ;; (love.graphics.setFont ((. assets.fonts "avara") 50))
  ;; (love.graphics.printf "Until the Morning Breaks" 270 500 450 :right)
)

(local col (require :prefab-col))

(fn game.draw [self]
  (love.graphics.setCanvas)
  (local player (get-player))
  (tset camera :x (-  player.pos.x 152))
  (tset camera :y (-  player.pos.y 74))
  (local drawn-snow (state.snow:draw))
  (state.snow.part:moveTo player.pos.x player.pos.y)
  (love.graphics.clear 0 0 0 1)
  (draw-ground)

  (love.graphics.push "all")
  (love.graphics.setCanvas draw-canvas)
  ;;(love.graphics.scale camera.scale)
  (love.graphics.translate (- (* 1 camera.x)) (- (* 1 camera.y)))
  (when state.debug
    (draw-debug))
  (each [_ object (pairs (lume.sort state.objects (fn [O1 O2]
                                                    (< (+ O1.pos.y O1.h)
                                                       (+ O2.pos.y O2.h))
                                                    )))] (object:draw))

  (love.graphics.draw drawn-snow)
  ;; (love.graphics.scale 1)
  ;;(love.graphics.translate 0 0)
  (when (and (not state.debug) (not (= :sandbox state.difficulty)))
    (love.graphics.setBlendMode :alpha :premultiplied)
    (love.graphics.draw (state.lights:draw camera)))
  (love.graphics.pop)

  ;; (love.graphics.setCanvas)
  ;; (love.graphics.setColor 1 1 1 1)
  ;;
  ;; (love.graphics.draw draw-canvas offsets.x offsets.y 0 camera.scale)

  (love.graphics.push "all")
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.setCanvas)
  (love.graphics.translate offsets.gx offsets.gy)
  (love.graphics.scale (* camera.scale offsets.s))
  (love.graphics.draw draw-canvas)
  (love.graphics.pop)
  ;; (tset (. state.objects 2) :time (* 2 60))

  ;; (love.graphics.clear 1 1 1 1)
  ;; (love.graphics.draw drawn-snow)
  ;; (love.graphics.setColor 1 0 0 1)
  ;; (love.graphics.printf (. state.objects 1 :state) 20 20 700 :left)

  ;; (when state.debug
  ;;    (col.draw state.world))
  ;; (love.graphics.draw state.level.tileset-batch 0 0 0 4)
  )

(fn game.keypressed [self key code]
  (match key
    :space (let [player (. state.objects 1)]
             (player:action))
    :escape (do
              (local gamestate (require :lib.gamestate))
              (assets.sounds.page:play)
              (gamestate.switch (require "mode-menu")))
    :tab (do
              (local gamestate (require :lib.gamestate))
              (assets.sounds.page:play)
              (gamestate.switch (require "mode-menu")))
    :return (do
              (local gamestate (require :lib.gamestate))
              (assets.sounds.page:play)
              (gamestate.switch (require "mode-menu")))
    :f10 (when (not state.web)
           (do
             (toggle-fullscreen)
             (tset state :snow (snow.new 10 10 20 100)) ;; bugs out when fs toggled
             ))
    :m (toggle-sound)
    :q (when (not state.web) (screenshot))
    :c   (do (assets.sounds.bounce:play) (when state.dev (gif:start)))
    :v   (do (assets.sounds.bounce:play) (when state.dev (gif:stop)))

    := (if state.debug (tset state :debug false) (tset state :debug true))
    ))


(fn game.keyreleased [self key code]
  (match key
    :space (let [player (. state.objects 1)]
             (player:anti-action))
    ))

(fn game.update [self dt]
  (state.snow:update dt)
  (gif:update dt)
  (state.lights:update dt)
  (when (not state.menu)
    (each [_ object (pairs state.objects)] (object:update dt)))
  (when (not false) (collectgarbage)))

(fn game.leave [self ...]
  (love.mouse.setVisible true))

(fn set-lights []
  (local final-light-canvas (love.graphics.newCanvas (* 2 1280) (* 2 720)))
  ;; (final-light-canvas:setFilter :nearest :nearest)
  (local player (get-player))
  (local fire   (get-fire))
  (local rings [[300 0.99]
                [240 0.94]
                [190 0.88]
                [150 0.75]
                [120 0.4]
                [100 0.2]
                [80 0]])
  (local points {:player {:x (+ player.pos.x player.center.x)
                          :y (+ player.pos.y player.center.y)
                          :radius-offset (player:strength)
                          :time 0 :period 5 :amplitude 2}
                 :fire {:x (+ fire.pos.x fire.center.x)
                        :y (+ fire.pos.y fire.center.y)
                        :radius-offset (fire:strength)
                        :time 0 :period 5 :amplitude 2}})
  (lights.new final-light-canvas params.colours.background rings points))

(fn game.enter [_self _from reset? difficulty?]
  (love.mouse.setVisible false)
  (set has-drawn-ground false)
  (tset state :menu false)
  (tset state :loaded true)
  (match reset?
    :continue nil
    _ (do
        (reset-state difficulty?)
        (tset state :snow (snow.new 10 10 20 100))
        ((require :level) _G.assets.sprites.KindredJam)
        (tset state :lights (set-lights))
        (state.lights.set-pos state.lights :player (get-player))
        )))

game
