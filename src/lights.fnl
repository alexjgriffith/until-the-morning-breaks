(local lights {})

(fn fade-on-edge []
  (local state (require :state))
  (local player (get-player))
  (local pos player.pos)
  (local [max-x max-y min-x min-y]
         (lume.map [(- state.level.w 35)
                    (- state.level.h 35)
                    35
                    35]
                   (fn [x] (* x 8))))
  ;; when 5 tiles from the edge begin fading out
  (local dx (- (math.min 0
                         (- max-x pos.x)
                         (- pos.x min-x)
                         (- max-y pos.y)
                         (- pos.y min-y))))
  (/ dx (* 10 8)))

(fn lights.draw [self camera]
  "Draw the lights to the canvas offset by x and y from camera. Returns a canvas."
  (local {: canvas : colour : rings : points} self)
  (local canvas (. self :canvas))
  (love.graphics.push "all")
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.setCanvas canvas)
  (love.graphics.clear colour)
  (love.graphics.setBlendMode :replace :premultiplied)
  (local [R G B _] self.colour)
  (each [_ [radius alpha] (ipairs rings)]
    (if (> alpha 2)
        (love.graphics.setColor  R G B alpha)
        (love.graphics.setColor  (/ (math.floor (* 128 (* alpha R))) 128)
                                 (/ (math.floor (* 128 (* alpha G))) 128)
                                 (/ (math.floor (* 128 (* alpha B))) 128)
                                 alpha))
    (each [_ {: x : y : radius-offset
              : time : period : amplitude}
           (pairs points)]
      ;; change ma function to change the breathing effect
      (local ma (* amplitude  2 (math.abs ( - time (/ period 2)))))
      (local sr (/ (+ ma radius radius-offset) camera.scale))
      (when (> sr 0)
        (love.graphics.circle "fill" (+ x camera.x) (+ y camera.y) sr 10))))
  (love.graphics.setBlendMode :alpha)
  (local fade-level (fade-on-edge))
  (local player (get-player))
  (when (> fade-level 0)
    (love.graphics.setColor R G B fade-level)
    (love.graphics.rectangle "fill" camera.x camera.y (* 2 1280) (* 2 1280)))
  (love.graphics.pop)
  canvas)

(fn lights.update [self dt]
  "Update the light timer."
  (local {: points} self)
  (each [_ point (pairs points)]
    (local time point.time)
    (tset point :time (+ dt time))
    (when (> time point.period)
      (tset point :time 0))))

(fn lights.set-pos [self name obj]
  "Update the position and strength of the light.

Obj must have keys pos and a meta function strength."
  (local light (. self :points name))
  (tset light :radius-offset (obj:strength))
  (tset light :x (+ obj.center.x obj.pos.x))
  (tset light :y (+ obj.center.y obj.pos.y)))

(local lights-mt {:__index lights
                  :set-pos lights.set-pos
                  :draw lights.draw
                  :update lights.update})

;; light {: x : y : radius-offest :time :period :amplatude}
(fn lights.new [canvas colour rings points]
  (setmetatable {: canvas : colour : rings : points} lights-mt))

lights
