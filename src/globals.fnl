(local assets _G.assets)

(global draw-canvas (love.graphics.newCanvas (/ 1280 4) (/ 720 4)))

(draw-canvas:setFilter :nearest :nearest)

(global menu-background (love.graphics.newCanvas 1980 1080))

(global offsets {:x 0 :y 0 :s 1 :sx 1 :sy 1 :gx 0 :gy 0})

(global camera {:x 0 :y 0 :scale 4})

(fn get-mouse-pos []
  (local (x y) (love.mouse.getPosition))
  [(- (/ x offsets.s) offsets.gx) (- (/ y offsets.s) offsets.gy)])

(fn get-mouse-pos-game-coord [camera]
  (local [x y] (get-mouse-pos))
  [(+ x camera.x) (+ y camera.y)])

(fn get-mouse-pos-game-scale [camera]
  (local [x y] (get-mouse-pos))
  [(- (/ x camera.scale) camera.x) (- (/ y camera.scale) camera.y)])

(global resize (fn []
  (local (x y flags) (love.window.getMode))
  (local sx (/ x 1280))
  (local sy (/ y 720))
  (tset offsets :sx sx)
  (tset offsets :sy sy)
  (tset offsets :s (math.min sx sy))
  (tset offsets :x (math.max 0 (-  (/ x 2) (/ flags.minwidth 2))))
  (tset offsets :y (math.max 0 (-  (/ y 2) (/ flags.minheight 2))))
  (if (< sx sy)
      (do (tset offsets :gx 0)
          (tset offsets :gy (/ (- y (* offsets.s 720)) 2)))
      (do
        (tset offsets :gx (/ (- x (* offsets.s 1280)) 2))
        (tset offsets :gy 0)))))


(global toggle-fullscreen (fn []
  (local (pw ph flags) (love.window.getMode))
  (local [w h] [flags.minwidth flags.minheight])
  (local (dx dy) (love.window.getDesktopDimensions flags.display))
  (if flags.fullscreen
      (do
        (tset flags :fullscreen false)
        (tset flags :centered true)
        (tset flags :x (math.max 0 (/ (- dx 1280) 2)))
        (tset flags :y (math.max 0 (/ (- dy 720) 2))))
      (do
        (love.window.setFullscreen true :desktop)
        (tset flags :fullscreen true)
        ))
  ;; (tset flags :centered true)
  (love.window.setMode w h flags)
  (local state (require :state))
  (when (and state state.snow)
    (tset state :snow (state.snow.new 10 10 20 100))) ;; bugs out when fs toggled)
  (resize)
  (local (cw ch _flags) (love.window.getMode))
  (tset offsets :x (math.max 0 (math.floor (-  (/ cw 2) (/ w 2)))))
  (tset offsets :y (math.max 0 (math.floor (-  (/ ch 2) (/ h 2)))))))

(global is-fullscreen
        (fn []
          (local (_w _h flags) (love.window.getMode))
          flags.fullscreen))

(global screenshot
        (fn []
          (assets.sounds.click:play)
          (love.graphics.captureScreenshot
           (string.format "screenshot-%s.png" (os.time)))))


(var start-mute false)
(tset _G :web-start-muted (fn [] start-mute))
(global web-start-unmute (fn [] (set start-mute false)))
(global web-start-mute (fn [] (set start-mute true)))

(var mute false)
(global toggle-sound
        (fn  []
          (web-start-unmute)
          (if mute
              (do
                (assets.sounds.click:play)
                (do (love.audio.setVolume 1) (set mute false)))
              (do  (love.audio.setVolume 0) (set mute true)))))

(global is-mute (fn [] mute))

(global set-mute (fn [in-mute]
                   (if in-mute
                         (do (love.audio.setVolume 0) (set mute true))
                         (do  (love.audio.setVolume 1) (set mute false)))))
