(local col {})

(local bump (require :lib.bump))

(fn col.draw [world]
  (love.graphics.push "all")
  (local (items len) (world:getItems))
  (love.graphics.setColor 1 0 0 1)
  (each [key value (pairs items)]
    (when (or (= :player value.type) (= :fire value.type)
              (= :egg value.type) (= :tree value.type)
              (= :revive value.type) (= :monster value.type)
              (= :dog value.type) (= :wood value.type)
              )
      (local (x y w h) (world:getRect value))
      (love.graphics.rectangle "line"
                               (+ offsets.x (* camera.scale (- x camera.x)))
                                (+ offsets.y (* camera.scale (- y camera.y)))
                               (* camera.scale w) (* camera.scale h))))
  (love.graphics.pop))


;; add tile
(fn col.add-tile [world mapin tile]
  (local collidable (. mapin.tile-set tile.type :collidable))
  (local [x y] [(* 8 tile.x) (* 8 tile.y)])
  (when (~= tile.type :ground) (world:add tile x y 8 8)))

;; create map
(fn col.new [mapin objects]
  (local world (bump.newWorld 32))
  (local tiles mapin.data.ground)
  (each [id tile (pairs tiles)]
    (col.add-tile world mapin tile))
  (each [id object (pairs objects)]
    (world:add object
               (+ object.col.x object.pos.x)
               (+ object.col.y object.pos.y)
               object.col.w
               object.col.h object.collision-type))

  world)

col
