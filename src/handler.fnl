(local state (require :state))

(local gamestate (require :lib.gamestate))

(local end-game (require :mode-end-state))

(local assets _G.assets)

(local howl (love.audio.newSource "assets/sounds/howl.ogg" :static))
(howl:setVolume 0.6)

(local birds (love.audio.newSource "assets/sounds/birds.ogg" :static))
(birds:setVolume 0.6)

(fn love.handlers.game-over [kind]
  ;; stop sounds
  (tset state :game-over kind)
  (_G.assets.sounds.land-trimmed:stop)
  (_G.assets.sounds.chop06:stop)
  (_G.assets.sounds.footsteps_snow:stop)
  (_G.assets.sounds.fire_crackling:stop)

  (match kind
    :gems (do
            (birds:play)
            (gamestate.switch end-game))
    :fireout (gamestate.switch end-game)
    :hunter (do (howl:play)
                (gamestate.switch end-game))))
