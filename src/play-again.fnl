(local state (require :state))
(local gamestate (require :lib.gamestate))

(local assets _G.assets)

(fn play-again [difficulty?]
  (when difficulty?
    (tset state :difficulty difficulty?))
  (local mode-game (require :mode-game))
  ;; (set timer 0)
  ;; (set step :leave)
  (assets.sounds.page:play)
  (reset-state state.difficulty)
  (mode-game.enter nil nil :reset (or difficulty? :default))
  (mode-game:update 0)
  (tset state :menu true)
  (_G.assets.sounds.footsteps_snow:play)
  (_G.assets.sounds.footsteps_snow:setVolume 0)
  (_G.assets.sounds.fire_crackling:play)
  (_G.assets.sounds.footsteps_snow:setVolume 0.6)
  (gamestate.switch (require "mode-menu") :end-game))

(fn repeat []
  ;; (set timer 0)
  ;; (set step :leave)
  (assets.sounds.page:play)
  (reset-state state.difficulty)
  (_G.assets.sounds.footsteps_snow:play)
  (_G.assets.sounds.footsteps_snow:setVolume 0)
  (_G.assets.sounds.fire_crackling:play)
  (_G.assets.sounds.footsteps_snow:setVolume 0.6)
  (gamestate.switch (require "mode-game") :new-game))

(fn play-hard []
  (play-again :hard))

(fn play-normal []
  (play-again :default))

(fn play-easy []
  (play-again :easy))

(fn play-sandbox []
  (play-again :sandbox))

(fn play-gg []
  (play-again :gg))

(fn same-difficulty []
  (play-again state.difficulty))

{:hard play-hard :normal play-normal
 :easy play-easy :sandbox play-sandbox
 :gg play-gg
 :same same-difficulty
 :repeat repeat}
