(local egg {})

(local loader (require :lib.loader))

(local hunter (require :prefab-hunter))

(local state (require :state))

(fn hatch [self atlas x y colour]
  (local colour-to-type {:red :pdog :blue :pen :green :fdog})
  ;;(howl:play)
  (tset self :type :crushed-egg)
  (tset self :name (.. :crushed-egg :- self.colour))
  (tset self :active false)
  (tset self :timer 0)
  (insert-object (hunter.new atlas x y (. colour-to-type colour) :wolf)))

(fn egg.crush [self]
  (tset self :active false)
  (tset self :name (.. :crushed-egg :- self.colour))
  (tset self :type :crushed-egg)
  (tset self :timer 0)
  (local new-egg (select-random-crushed-egg))
  (when new-egg
    (new-egg:activate self.colour)
    true))

(fn to-active-state [self]
  (tset self :respawining false)
  (tset self :type :egg)
  (tset self :name (.. :egg :- self.colour))
  (tset self :active true)
  (tset self :timer 0))

(fn egg.collide [egg])

(fn active-state-update [self dt]
  (tset self :respawning false)
  (tset self :timer (+ (* dt (. (require :state) :hatch-rate)) self.timer))
  (when (and self.not-howl self.active (> self.timer (- self.hatch-time 2)))
    (tset self :not-howl false)
    (self.crack:play))
  (when (and self.active (> self.timer self.hatch-time))
    (hatch self self.atlas self.pos.x self.pos.y self.colour)))

(fn respawn-state-update [self dt]
  (tset self :respawning true)
  (local player (get-player))
  (local out-of-sight (> (* 8 20)
                         (lume.distance self.pos.x self.pos.y player.pos.x player.pos.y)))
  (when self.time-to-respawn
    (tset self :timer (+ (* dt (. (require :state) :hatch-rate)) self.timer))
    (when  (and out-of-sight (> self.timer self.time-to-respawn))
      (to-active-state self))))

(fn egg.update [self dt]
  (tset self :respawning false)
  (match self.type
    :egg (active-state-update self dt)
    :crushed-egg (respawn-state-update self dt)))

(fn egg.draw [self]
  (love.graphics.setColor 1 1 1 1)
  (when self.active
    (love.graphics.draw self.image self.egg-quad
                      self.pos.x
                      self.pos.y)))

(local egg-mt {:__index egg
                :update egg.update
                :collide egg.collide
               :draw egg.draw
               :activeate egg.activate})

(fn new-quad [atlas member]
  (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
    (love.graphics.newQuad  x y w h 256 256)))

(fn egg.activate [self colour]
  (tset self :colour colour)
  (tset self :active false)
  (tset self :type :crushed-egg)
  (tset self :name (.. :crushed-egg- colour))
  (local egg-quad (new-quad self.atlas (.. colour :-egg)))
  (tset self :egg-quad egg-quad)
  (tset self :timer 0)
  (tset self :respawning true)
  (tset self :time-to-respawn (or state.egg-respawn false)))

(fn egg.new [atlas x y colour? type?]
  (local type (or type? :egg))
  (local active (= type :egg))
  (local colour (or colour? :blue))
  (local egg-quad (new-quad atlas (.. colour :-egg)))
  (local assets _G.assets)
  (local crack (love.audio.newSource "assets/sounds/crack.ogg" "static"))
  (crack:setVolume 0.6)
  (setmetatable {:pos {: x : y}
                 :image atlas.image
                 : atlas
                 :time-to-respawn false
                 : egg-quad
                 :w 16 :h 16 :collision-type :slide
                 :col {:x 2 :y 8 :w 12 :h 8}
                 :type type
                 :colour colour
                 :timer 0
                 :not-howl true
                 : crack
                 :respawning false
                 :hatch-time (lume.random 30 60)
                 :name (.. type :- colour)
                 :active active} egg-mt))

egg
