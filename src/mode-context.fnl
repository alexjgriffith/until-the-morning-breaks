(local controls {})

(local gamestate (require :lib.gamestate))
(local buttons (require :lib.buttons))
(local params (require :params))

(local state (require :state))

(local assets _G.assets)

(local controls-text
       "
The world has been cursed and engulfed in darkness.
You must collect the three gems of morning and bring
them to your bonfire.

Don't let the fire go out. There are evil things
hatching out in the darkness. Evil things that
prey on poor small animals, lost in the dark.
")


(local elements
       [{:type :title :y 50 :w 400 :h 60 :text "Until the Morning Breaks"}
        {:type :button :y 600 :oy -10 :ox 0 :w 400 :h 70 :text "Back" }
        {:type :small-text-left :y 200 :x 200 :oy -10 :ox 100 :w 900 :h 70 :text controls-text}])

(local element-font
       {:title  ((. assets.fonts "avara") 90)
        :subtitle  ((. assets.fonts "avara") 40)
        :button  ((. assets.fonts "avara") 40)
        :small-text-left  ((. assets.fonts "inconsolata") 35)})

(local element-click
       {"Back"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-menu") :controls))})

(local element-hover {:button (fn [element x y] :nothing)})

(local ui (buttons elements params element-click element-hover element-font))

(local menu-canvas (love.graphics.newCanvas 1280 720))
(fn controls.draw [self]
  (love.graphics.setCanvas menu-canvas)
  (love.graphics.setColor params.colours.blue-grey)
  (love.graphics.rectangle "fill" 0 0 1280 720)
  (love.graphics.setColor 1 1 1 1)
  (ui:draw)
  (love.graphics.setCanvas)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.clear params.colours.blue-grey)
  (love.graphics.draw menu-canvas offsets.x offsets.y))

(fn controls.update [self dt]
  (ui:update dt))

(fn controls.mousereleased [self ...]
  (ui:mousereleased ...))

(fn controls.keypressed [self key code]
  (match key
    :f10 (when (not state.web) (toggle-fullscreen))
    "escape"  (gamestate.switch (require :mode-menu))
    "tab"  (gamestate.switch (require :mode-menu))
    "enter"  (gamestate.switch (require :mode-menu))
    "m" (toggle-sound)
    "q" (when (not state.web) (screenshot))
    ))

controls
