(local colour (require :lib.colour))

{:colours {:blue   (colour.hex-to-rgba "#5fcde4")
           :black  (colour.hex-to-rgba "#222034")
           :red    (colour.hex-to-rgba "#ac3232")
           :grey   (colour.hex-to-rgba "#595652")
           :blue-grey   (colour.hex-to-rgba "#9babb7")
           :white  (colour.hex-to-rgba "#ffffff")
           :pink  (colour.hex-to-rgba "#d77bba")
           :background  (colour.hex-to-rgba "#222034")
           :light-yellow  (colour.hex-to-rgba "#cbdbfc")
           :text  (colour.hex-to-rgba "#222034")
           }
 :screen-width 1280
 :screen-height 720}
