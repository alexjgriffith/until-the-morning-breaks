(local tree {})

(local loader (require :lib.loader))

(fn tree.collide [tree])

(fn tree.update [self dt])

(fn tree.draw [self]
  (love.graphics.setColor 1 1 1 1)
  (local quad (if self.active self.tree-quad self.stump-quad))
  (local oy (if self.active 0 32))
  (love.graphics.draw self.image quad
                      self.pos.x
                      (+ oy self.pos.y)))

(local tree-mt {:__index tree
                :update tree.update
                :collide tree.collide
                :draw tree.draw})

(fn tree.new [atlas x y i]
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 256 256)))
  (local tree-quad (new-quad :tree))
  (local stump-quad (new-quad :trunk))
  (setmetatable {:pos {: x : y}
                 :image atlas.image
                 : tree-quad
                 : stump-quad
                 :w 16 :h 48 :collision-type :slide
                 :col {:x 2 :y 40 :w 12 :h 8}
                 :type :tree
                 :name (.. :tree- i)
                 :active true} tree-mt))

tree
