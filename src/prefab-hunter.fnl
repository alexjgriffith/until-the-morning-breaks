(local loader (require :lib.loader))
(local movement (require :lib.movement))
(local vec2d (require :vec2d))

(local hunter {})

(local foots (require :footsteps))

(local default-collide
       {:fire "slide"
        :chasm "slide"
        :tree "slide"
        :egg "slide"
        :dog "slide"
        :hunter "cross"
        :wood "cross"
        :revive "cross"
})

(fn hunter.collide [hunter other]
  (. default-collide other.type))

(fn weighting [root pos min max]
  (lume.clamp (/ (- (lume.distance root.x root.y pos.x pos.y) min)
     (- max min)) 0 1))

(fn weighting-batch [root pos points weights min max]
  (vec2d.batch-shape root points weights {:weight (weighting root pos min max)
                                          :dpos pos}))

(fn steering [self]
  (local state (require :state))
  (local avoid-dist 20)
  (local search-dist 20)
  (local edge-dist 4)
  (local pos {:x (+ self.pos.x self.col.x 4)
              :y (+ self.pos.y self.col.y)})
  (tset self :dpos pos)
  (tset self :angle  (+ self.angle  (*  0.1 (- (love.math.noise
                                                (+ self.rv
                                                   (lume.random 0 0.1)
                                                   (* self.ni 0.001))) 0.5))))
  (tset self :ni (+ self.ni 1))
  (fn make-wander-goal [{: x : y} angle]
    (let [dx (math.cos self.angle)
          dy (math.sin self.angle)]
      {:x (+ x dx -4) :y (+ y dy)}))
  (local wander-goal {:pos (make-wander-goal pos self.angle) :col {:x 0 :y 0}})
  (local voids-objs-man (state.world:queryRect
                (- pos.x (* avoid-dist 8) (- 8))
                (- pos.y (* avoid-dist 8) (- 24 ) )
                (* avoid-dist 2 8)
                (* avoid-dist 2 8)
                (fn filter [item]
                  (match item.type
                    :tree true
                    :egg  true
                    :dog  true
                    :fire true
                    :hunter true
                    ;; :chasm true
                    _ false))))

  (local voids-edge-man (state.world:queryRect
                         (- pos.x (* edge-dist 8))
                         (- pos.y (* edge-dist 8))
                         (* edge-dist 2 8)
                         (* edge-dist 2 8)
                         (fn filter [item]
                           (match item.type
                             :chasm true
                             _ false))))
  (local voids-man {})
  (when voids-objs-man
    (each [_ obj (ipairs voids-objs-man)]
    (table.insert voids-man obj)))
  (when voids-edge-man
    (each [_ obj (ipairs voids-edge-man)]
      (table.insert voids-man obj)))
  (local goals {})
  (local player (. state.objects 1))
  (local fire (. state.objects 2))
  (local player-distance (lume.distance self.pos.x self.pos.y player.pos.x player.pos.y))
  (local fire-distance (lume.distance self.pos.x self.pos.y fire.pos.x fire.pos.y))
  (when (< player-distance (* 8 2))
    ;; (tset state :hunter (. {:pen "The Flightless Horror"
    ;;                         :pdog "The Midnight Wolf"
    ;;                         :fdog "Fluffy the Beautiful"} self.creature)
    (local name-gen (require :names))
    (tset state :hunter
          (name-gen (. {:pen :bird :pdog :wolf :fdog :wolf} self.creature)))
    (love.event.push :game-over :hunter)
    :game-over-hunter)
  (local previous self.state)
  (if
   (< fire-distance (* 8 15))
   (do
     (tset goals 1 wander-goal)
     (tset self :state :wander)
     (tset self :max-speed (* 60 0.5)))

   (or (< player-distance (* 8 8)))
   (do (tset goals 1  player)
       (tset self :state :chase)
       (tset self :max-speed (* 60 1.3)))

   (and (= player.state :walk) (< player-distance (* 8 10)))
   (do (tset goals 1  player)
       (tset self :state :chase)
       (tset self :max-speed (* 60 1.5)))

   (and (= player.state :up-walk) (< player-distance (* 8 10)))
   (do (tset goals 1  player)
       (tset self :state :chase)
       (tset self :max-speed (* 60 3)))

   (or (and (= player.state :run) (< player-distance (* 8 20)))
       (and  (= :chase self.state) (< player-distance (* 8 20))))
   (do (tset goals 1  player)
       (tset self :state :chase)
       (tset self :max-speed (* 60 1.5)))

   (and (= player.state :chop)
        (< player-distance (* 8 40)))
   (do (tset goals 1  player)
       (tset self :state :chase)
       (tset self :max-speed (* 60 1.5)))

   ;; add chase animals

   (do (tset goals 1 wander-goal)
       (tset self :state :wander)
       (tset self :max-speed (* 60 0.5))))

  (match [previous self.state]
    [:chase :chase] false
    [_ :chase] (do
                 (pp [previous self.state])
                 (tset self :stall-timer 0)
                 (self.howl:play)))
  (fn calc-weight [pa pb sq]
    (if (> (/ 1 (math.max 0.00001
                      (lume.distance
                       pa.x pa.y pb.x pb.y)))
             (/ 1 (* sq 8)))
      1
      0))

  (local void-objects (lume.map voids-man
                          (fn [obj]
                            (match obj.type
                              :fire (do (tset obj :dpos
                                              {:x (+ obj.pos.x obj.col.x 4)
                                               :y (+ obj.pos.y obj.col.y)})
                                        (tset obj :steering-weight
                                          (calc-weight pos obj.dpos 15)))
                              :hunter (do (tset obj :dpos
                                              {:x (+ obj.pos.x obj.col.x 4)
                                               :y (+ obj.pos.y obj.col.y)})
                                        (tset obj :steering-weight
                                              (calc-weight pos obj.dpos 2)))
                              :player (do (tset obj :dpos
                                              {:x (+ obj.pos.x obj.col.x 4)
                                               :y (+ obj.pos.y obj.col.y)})
                                        (tset obj :steering-weight
                                              (calc-weight pos obj.dpos 3)))
                              :chasm (do
                                       (tset obj :dpos
                                              {:x (+ (* 8 obj.x) 4)
                                               :y (+ (* 8 obj.y) 4)})
                                        (tset obj :steering-weight
                                              (calc-weight pos obj.dpos 10))
                                        ;; (pp obj)
                                        )
                              _ (do (tset obj :dpos
                                          {:x (+ obj.pos.x obj.col.x 8)
                                           :y (+ obj.pos.y obj.col.y)})
                                    (tset obj :steering-weight
                                          (calc-weight pos obj.dpos 5))))
                            obj)))
  (local mask (vec2d.batch-dot-products pos self.points void-objects))
  (tset self :voids void-objects)
  (local goal-objects (lume.map goals
                           (fn [obj]
                             (match obj.type
                               _ (do (tset obj :steering-weight 1)
                                           (tset obj :dpos
                                                 {:x (+ obj.pos.x obj.col.x 4)
                                                  :y (+ obj.pos.y obj.col.y)})))
                             obj)))
  (tset self :goals goal-objects)
  (local chase-unweighted (vec2d.batch-dot-products pos self.points goal-objects))
  (local chase
         (if (= self.state :chase)
             chase-unweighted
             (weighting-batch pos self.home self.points chase-unweighted (* 8 5) (* 8 20))))
  (var chase-max 0.000001)
  (each [i c (ipairs chase)]
    (when (> c chase-max)
      (set chase-max c)))
  (each [i c (ipairs chase)]
    (tset chase i (/ c chase-max))
    (when (> (. mask i) 0)
      (tset chase i 0)))
  (local weights chase)
  (tset self :weights weights)
  (vec2d.batch-highest-weight self.points weights))

(fn moving [self dt]
  (local state (require :state))
  (local world state.world)
  (let [{:x dx :y dy} (steering self)
        x (+ self.pos.x (* self.max-speed dt dx))
        y (+ self.pos.y (* self.max-speed dt dy))]
    (local (ax ay cols len) (world:move self
                                        (+ x self.col.x)
                                        (+ y self.col.y)
                                        self.collide))
  (if (or (~= ax (+ x self.col.x))
            (~= ay (+ y self.col.y)))
      (do (tset self.speed :x 0)
          (tset self.speed :y 0))
      (do (tset self.pos :x x)
          (tset self.pos :y y)))

    ))

(fn hunter.update [self dt]
  (when (>= self.stall-timer self.stall-time)
      (moving self dt))
  (tset self :stall-timer (+ self.stall-timer dt))
  (self.footsteps:update {:x (+ 8 self.pos.x) :y (+ 30 self.pos.y)} self.angle))

(fn hunter.draw [self]
  (love.graphics.setColor 1 1 1 0.5)
  (love.graphics.draw self.image self.shadow-quad
                      self.pos.x
                      (+ self.pos.y (- self.h 14)))
  (love.graphics.setColor 1 1 1 1)
  (self.footsteps:draw)
  (love.graphics.draw self.image self.quad
                      self.pos.x
                      self.pos.y))

(local hunter-mt {:__index hunter
                :update hunter.update
                :collide hunter.collide
                :draw hunter.draw})

(fn hunter.new [atlas x y creature level]
  (local state (require :state)) ;; needed for params
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 256 256)))
  (local name (.. creature "-" level))
  (local shadow-quad (new-quad :shadow))
  (local quad (new-quad name))
  (local assets _G.assets)
  (local howl (love.audio.newSource "assets/sounds/scifi.ogg" "static"))
  (local footsteps (foots.new (+ x 8) (+ y 8) atlas.image
                              (new-quad (.. :footstep- creature :-left))
                              (new-quad (.. :footstep- creature :-right))
                              20
                              20))
  (howl:setVolume 0.6)
  (setmetatable {:pos {:x x :y y}
                 :speed {:x 0 :y 0 :rate 20 :decay 40
                         :max 2 :max-s2 (/ 2 (math.sqrt 2)) :floor 0.2}
                 :image atlas.image
                 : footsteps
                 ;; : sprite
                 : shadow-quad
                 : quad
                 : name
                 : level
                 : creature
                 : howl
                 :voids []
                 :goals []
                 :weights nil
                 :points (vec2d.batch-angle-to-point-unit)
                 :type :hunter
                 :w (if (~= level "wolf") 16 16)
                 :h (if (~= level "wolf") 16 32)
                 :col (if (~= level "wolf")
                          {:x 4 :y 8 :w 8 :h 8}
                          {:x 4 :y 24 :w 8 :h 8})
                 :home {:x x :y y}
                 :ni 1
                 :ns 0.001
                 :na 1
                 :stall-timer (or state.stall-time 0)
                 :stall-time (or state.stall-time 0)
                 :max-speed 0.3
                 :state :wander
                 :collision-type :cross
                 :rv (lume.random 0 10000)
                 :angle (lume.random 1 (* 2 math.pi))
                 }
                hunter-mt))

hunter
