(global gamestate (require :lib.gamestate))
(global params (require :params))

(local repl (require :lib.stdio))
(local cargo (require :lib.cargo))

(require :globals)

(math.randomseed (os.time))

(local state (require :state))

(when (not false) (collectgarbage "stop"))

(require :globals)

(fn love.load []
  (when state.web (_G.web-start-mute))
  (love.graphics.setBackgroundColor params.colours.black)
  (love.filesystem.setIdentity "until-the-morning-breaks")
  (when (not (love.filesystem.getInfo (love.filesystem.getSaveDirectory)))
    (love.filesystem.createDirectory (love.filesystem.getSaveDirectory))
    (pp (.. (love.filesystem.getSaveDirectory) " created.")))
  (love.graphics.setDefaultFilter "nearest" "nearest")
  (tset _G :assets (cargo.init {:dir :assets
                                :processors
                                {"sounds/"
                                 (fn [sound _filename]
                                   (sound:setVolume 0.1)
                                   sound)
                                 }}))
  (local bgm (. _G.assets.sounds "fire_crackling"))
  (bgm:setLooping true)
  (if (_G.web-start-muted)
      (bgm:setVolume 0)
      (bgm:setVolume 0.25))
  (bgm:play)

  (local foot (. _G.assets.sounds "footsteps_snow"))
  (foot:setLooping true)
  (foot:setVolume 0)
  (foot:setPitch 0.5)
  (foot:play)
  (_G.assets.sounds.bounce:setVolume 0.1)
  (require :handler)
  (gamestate.registerEvents)
  (gamestate.switch (require :mode-menu) :wrap)
  (when (not state.web) (repl.start))
  )


(fn love.resize [_w _h]
  (when (not state.web) (_G.resize)))
