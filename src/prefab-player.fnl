(local loader (require :lib.loader))
(local movement (require :lib.movement))

(local state (require :state))

(local assets _G.assets)

(local player {})

(local keybindings {:left  [:a :left] :right [:d :right]
                    :down  [:s :down] :up    [:w :up]
                    :space [:space]})

(local prefab-wood (require :prefab-wood))
(local prefab-gem (require :prefab-gem))

(local foots (require :footsteps))

(local default-collide
       {:fire "slide"
        :chasm "slide"
        :tree "slide"
        :egg "slide"
        :crushed-egg "cross"
        :dog "cross"
        :monster "cross"
        :wood "cross"
        :revive "cross"
})

(fn player.collide [player other]
  (. default-collide other.type))

(local gem-timer {:timer 0
                  :period 0.25
                  :rises [1 0 -1 -1 0 2 2 2]
                  :index 1})

(fn update-timer [dt obj]
  (local {: period : rises} obj)
  (local next-time (+ dt obj.timer))
  (if (> next-time period)
      (do
        (tset obj :index (+ obj.index 1))
        (when (> obj.index (# rises))
          (tset obj :index 1))
        (tset obj :timer (- next-time period))
        )
      (tset obj :timer next-time)))

(fn get-rise [{: rises : index}]
  (. rises index))

(fn update-crackle [self]
  (local state (require :state))
  (local fire (get-fire))
  (local distance (lume.distance self.pos.x self.pos.y fire.pos.x fire.pos.y))
  (local normalized (- 0.3 (/ distance 1000)))
  (when (not (_G.web-start-muted))
    (local bgv (lume.clamp normalized 0.005 0.5))
    ;; (pp [distance normalized bgv])
    (local bgm (. _G.assets.sounds "fire_crackling"))
    (if (~= :out fire.level)
        (bgm:setVolume bgv)
        (bgm:setVolume 0))
    (local footsteps (. _G.assets.sounds "footsteps_snow"))
    (footsteps:setVolume (math.max 0 (- 0.3 (* 3 bgv)))))
  )

(fn play-footsteps []
  (local sound (. _G.assets.sounds "land-trimmed"))
  (sound:setLooping true)
  (sound:setVolume 0.05)
  ;; (sound:setPitch 0.5)
  (sound:stop)
  (sound:play))

(fn play-chopping [yes]
  (local sound (. _G.assets.sounds "chop06"))
  (match [yes (sound:isPlaying)]
    [true true] :good
    [true false] (do
                   (sound:setLooping true)
                   (sound:play))
    [false true] (sound:setLooping false)
    [false false] :good))

(fn reset-footsteps []
  (local sound (. _G.assets.sounds "land-trimmed"))
  (sound:setLooping false))

(fn start-stop [self state]
  (fn to-move [state]
    (local map {:walk :move :run :move :up-walk :move})
    (match (. map state)
      :move :move
      _     :still))
  (match [(to-move state) self.play-footsteps]
    [:still true] (do (reset-footsteps)
                      (tset self :play-footsteps false))
    [:move false] (do (play-footsteps)
                      (tset self :play-footsteps true))
    [:move true] (tset self :play-footsteps true)
    [:still false] (reset-footsteps)
    )
  )

(fn update-front [self]
  (local pos self.pos)
  (tset self :front
        (match self.dir
          :forward  {:l (- pos.x 8) :t (+ pos.y 24) :w 32 :h 16}
          :right    {:l (+ pos.x 8) :t (+ pos.y 8) :w 24 :h 32}
          :backward {:l (- pos.x 8) :t (- pos.y 0) :w 32 :h 24}
          :left     {:l (- pos.x 16) :t (+ pos.y 8) :w 24 :h 32})))

(fn player.action [self]
  (local state (require :state))
  (local world (. state.world))
  (let [{: l : t : w : h} self.front]
    (local (items len)
           (world:queryRect
            l t w h
            (fn [item]

              (and item.type (or (and (= item.type :tree) item.active)
                                 (and (= item.type :egg) item.active)
                                 (= item.type :wood)
                                 (= item.type :fire)
                                 (= item.type :gem))))))
    (var tree nil)
    (var egg  nil)
    (var wood nil)
    (var fire nil)
    (var gem nil)
    (each [key item (pairs items)]
      (if (= item.type :tree) (set tree item)
          (= item.type :wood) (set wood item)
          (= item.type :fire) (set fire item)
          (= item.type :egg ) (set egg  item)
          (= item.type :gem ) (set gem  item)))
    (if
     ;; crush egg
     (and egg self.hands-free)
     (do
       (tset self :state :chop)
       (tset self.speed :x 0)
       (tset self.speed :y 0)
       (each [key obj (pairs state.objects)]
         (when (= obj.name egg.name)
           (tset self :target-egg obj)))
          (when (not self.target-egg) ;; couldn't find the tree
            (tset self :state :base)))  ;; for some reason

     ;; pick up gem
     (and gem self.hands-free)
     (do
       (local gem-found (get-object-by-name gem.name))
       (when gem-found
         (: (. _G.assets.sounds "bounce") :play)
         (gem-found:decrement)
         (tset self :hands-free false)
         (tset self :gem gem-found.colour)
         (tset self :state :up)))

     ;; pick up wood
     (and wood self.hands-free)
     (do
       (local wood-pile (get-object-by-name wood.name))
       (when wood-pile
         (: (. _G.assets.sounds "bounce") :play)
         (wood-pile:decrement)
         (tset self :hands-free false)
         (tset self :state :up)))

     ;; chop down the tree
     (and tree self.hands-free)
     (do
       (tset self :state :chop)
       (tset self.speed :x 0)
       (tset self.speed :y 0)
       (each [key obj (pairs state.objects)]
            (when (= obj.name tree.name)
              (tset self :target-tree obj)))
       (when (not self.target-tree) ;; couldn't find the tree
         (tset self :state :base)))  ;; for some reason

     ;; try to throw in the fire (gem or log)
     (and fire (not self.hands-free))
     (if self.gem
         (do
           (local f (get-fire))
           (f.add-gem f self.gem)
           (: (. _G.assets.sounds "bounce") :play)
           (tset self :state :base)
           (tset self :hands-free true)
           (tset self :gem nil)
           )
         (let [log-added? (: (. state.objects 2) :add-log)]
           (when log-added?
             (: (. _G.assets.sounds "fire-light") :setVolume 0.4)
             (: (. _G.assets.sounds "fire-light") :play)
             (tset self :state :base)
             (tset self :hands-free true))))

     ;; add to pile
     (and wood (not self.hands-free) (not self.gem))
     (do
       (local wood-pile (get-object-by-name wood.name))
       (when (and wood-pile (wood-pile:increment))
         (: (. _G.assets.sounds "bounce") :play)
         (tset self :hands-free true)
         (tset self :state :base)))

     ;; drop on ground
     (and (not self.hands-free))
     (do (: (. _G.assets.sounds "bounce") :play)
         (if self.gem
             (do (insert-object (prefab-gem.new self.atlas self.pos.x (+ self.pos.y 12)
                                                self.gem))
                 (tset self :gem nil))
             (insert-object (prefab-wood.new self.atlas self.pos.x (+ self.pos.y 12))))
         (tset self :hands-free true)
         (tset self :state :base))
     )))

(fn player.anti-action [self]
  (if (= self.state :chop)
      (tset self :state :base)))

(fn player.strength [{: time : pos}]
  (local fire (get-fire))
  (local dist (math.max 0
                        (- (lume.distance pos.x pos.y fire.pos.x fire.pos.y)
                           (* 8 20))))
  (+ -100
     (- (* 210 (- 1 (/ time (* 0.5 60)))))
     ;; (* 2 dx)
     ))

(fn player.update [self dt]
  (when (<= self.time 0)
    (love.event.push :game-over :fireout)
    :game-over-fireout)
  (tset self :last-state self.state)
  (local fire (get-fire))
  (if (= 0 fire.time)
      (tset self :time (math.max 0 (- self.time (* dt state.burn-rate))))
      (tset self :time (* 0.5 60)))
  (fn player-update-controler [dt]
    (match [(love.keyboard.isDown "space") self.state self.hands-free]
      [_ _ false] false
      [true :walk] (tset self :state :run)
      [true :run] (do :run)
      [false :run] (tset self :state :walk)
      _ false)
    (match self.state
      :run (do
             (tset self.speed :max self.run-speed)
             (tset self.speed :max-s2 (/ self.run-speed (math.sqrt 2))))
      _ (do (tset self.speed :max self.walk-speed)
            (tset self.speed :max-s2 (/ self.walk-speed (math.sqrt 2)))))
    (let [(_x _y dir moving) (if (= self.state :chop)
                                 (values nil nil self.dir false)
                                 (movement.topdown dt keybindings self))
          state (match [moving self.state self.hands-free]
                  [_    :chop _   ]  :chop
                  [true :run  true] :run
                  [true  _    true] :walk
                  [false _    true] :base
                  [true  _    false] :up-walk
                  [false _    false] :up
                  )
          anim (.. dir "-" state)]
      (tset self :state state)
      (when (~= anim self.anim)
        (self.sprite:reset anim)
        (tset self :anim anim))
      (when moving
        (tset self :angle (math.atan2 self.speed.x self.speed.y)))
      anim))
  (local state (require :state))
  (local world state.world)
  (local (ax ay cols len) (world:move self
                                      (+ self.pos.x 4)
                                      (+ self.pos.y 24)
                                      self.collide))
  (when (or (~= ax (+ self.pos.x 4))
            (~= ay (+ self.pos.y 24)))
      (tset self.speed :x 0)
      (tset self.speed :y 0))
  (tset self.pos :x (- ax 4))
  (tset self.pos :y (- ay 24))
  (update-front self)
  (if (and self.target-egg (= self.state :chop))
      (tset self :crush-time (+ self.crush-time dt))
      (tset self :crush-time 0))
  (when (> self.crush-time self.crush-at)
    (self.target-egg:crush)
    (tset self :target-egg nil)
    (tset self :state :base)
    (tset self :crush-time 0))

  (if (and self.target-tree (= self.state :chop) )
      (tset self :chop-time (+ self.chop-time dt))
      (tset self :chop-time 0))
  (when (> self.chop-time self.cut-down-at)
    (tset self.target-tree :active false)
    (tset self :target-tree nil)
    (tset self :state :up)
    (tset self :hands-free false)
    (tset self :chip-time 0))
  (update-crackle self)
  (let [anim (player-update-controler dt)]
    (self.sprite:update anim dt))
  (play-chopping (= self.state :chop))
  (start-stop self self.state)
  (update-timer dt gem-timer)
  (state.lights.set-pos state.lights :player self)
  (self.footsteps:update {:x (+ 8 self.pos.x) :y (+ 30 self.pos.y)} self.angle))

(fn player.draw [self]
  (love.graphics.setColor 1 1 1 1)
  (when (not self.hands-free)
    (if (= nil self.gem)
        (love.graphics.draw self.image self.wood-quad
                            (+ self.pos.x )
                            (- self.pos.y 8))
        (love.graphics.draw self.image (. self.gem-quads self.gem)
                            (+ self.pos.x 4)
                            (- self.pos.y 4 (get-rise gem-timer)))
        )

    )
  (love.graphics.setColor 1 1 1 0.5)
  (love.graphics.draw self.image self.shadow-quad
                      self.pos.x
                      (+ self.pos.y 18))
  (love.graphics.setColor 1 1 1 1)
  (self.footsteps:draw)
  (self.sprite:draw self.anim self.pos 1)
  ;; (love.graphics.rectangle "line" self.front.l self.front.t self.front.w self.front.h)
  )


(local player-mt {:__index player
                :update player.update
                :collide player.collide
                :draw player.draw})

(fn player.new [atlas x y]
  (local sprite (loader.load-four "assets/data/player" "assets/sprites" 16 32 7))
  (local run-sprite (loader.load-four "assets/data/player-run" "assets/sprites" 16 32 7 4))
  (each [key value (pairs run-sprite.animations)]
    (tset sprite.animations key value))
  ;; (pp sprite.animations)
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 256 256)))
  (local shadow-quad (new-quad :shadow))
  (local wood-quad (new-quad :logs-1))
  (local footsteps (foots.new (+ x 8) (+ y 30) atlas.image
                                  (new-quad :footstep-left)
                                  (new-quad :footstep-right)
                                  15
                                  10))
  (local gem-quads {:red (new-quad :red-gem)
                    :blue (new-quad :blue-gem)
                    :green (new-quad :green-gem)})
  (setmetatable {:pos {:x x :y y}
                 :angle 0
                 :walk-speed 2
                 :run-speed 3
                 :speed {:x 0 :y 0 :rate 10 :decay 40
                         :max 2 :max-s2 (/ 2 (math.sqrt 2)) :floor 1}
                 :states [:base :walk :run :chop :up :up-walk :drop]
                 :dir  :forward
                 :state :base
                 :last-state :base
                 :anim :forward-base
                 :image atlas.image
                 : footsteps
                 : atlas
                 : sprite
                 : shadow-quad
                 : wood-quad
                 : gem-quads
                 :play-footsteps false
                 :type :player
                 :hands-free true
                 :gem nil
                 :target-egg nil
                 :crush-time 0
                 :crush-at 1
                 :target-tree nil
                 :chop-time 0
                 :cut-down-at 2.80
                 :w 16
                 :h 32
                 :time (* 0.5 60)
                 :front {:l x :t x :w 0 :h 0}
                 :center {:x 8 :y 16}
                 :col {:x 4 :y 24 :w 8 :h 8}}
                player-mt))

player
