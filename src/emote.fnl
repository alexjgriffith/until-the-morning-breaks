(local emote {})

(local anim-timer {:timer 0
                   :period 0.4
                   :rises [1 2]
                   :index 1
                   :rcount 5
                   :r 1})

(fn update-timer [dt obj]
  (local {: period : rises} obj)
  (local next-time (+ dt obj.timer))
  (if (> next-time period)
      (do
        (tset obj :index (+ obj.index 1))
        (when (> obj.index (# rises))
          (tset obj :r (+ 1 obj.r))
          (tset obj :index 1))
        (tset obj :timer (- next-time period)))
      (tset obj :timer next-time)))

(fn get-rise [{: rises : index}] (. rises index))

(fn emote.update [self dt obj]
  (when self.active
    (tset self :pos obj.pos)
    (update-timer dt anim-timer)
    (when (and (not self.repeat) (>= anim-timer.r anim-timer.rcount))
      (tset self :active false))))

(fn emote.draw [self]
  (when self.active
    (love.graphics.draw self.image
                      (. self.quads self.active-emote (get-rise anim-timer))
                      (+ self.pos.x self.off.x)
                      (+ self.pos.y self.off.y))))

(fn emote.set [self active-emote repeat]
  (tset self :active-emote active-emote)
  (tset anim-timer :timer 0)
  (tset anim-timer :index 1)
  (tset anim-timer :r 1)
  (tset self :repeat repeat)
  (tset self :active true))

(fn emote.end [self active-emote]
  (when (or (not active-emote)
            (= self.active-emote active-emote))
    (tset self :repeat false)
    (tset self :active false)))

(local emote-mt {:__index emote
                 :update emote.update
                 :draw emote.draw
                 :set emote.set
                 :end emote.end})

(fn emote.new [atlas x y pos quads]
  (setmetatable {:image atlas.image
                 : pos
                 :off {: x : y}
                 : quads
                 :repeat false
                 :active false
                 } emote-mt))

emote
