(local lgen (require :lgen))

(fn []
  (local main (lgen.choose-n (lgen.select-square 3 3 1) 1))
  (local [[main-x main-y]] main)
  (local rim (lgen.subtract (lgen.select-square main-x main-y 1) main))
  (local outer (lgen.not (lgen.select-square main-x main-y 1) 5 5))
  (local eggs (lgen.choose-n outer 3))
  (local [[dog-x dog-y]] (lgen.choose-n outer 1))
  (local [[revive-x revive-y]] (lgen.choose-n (lgen.subtract outer eggs) 1))
  (local trees-rim (lgen.choose-n rim 3))
  (local trees-outer (lgen.choose-n (lgen.subtract outer eggs) 8))
  (local dogs (lgen.choose-n (lgen.subtract outer eggs) 6))
  (local trees (lgen.union trees-rim trees-outer))
  (local gems (lgen.choose-n (lgen.subtract outer eggs) 3))
  (local str {})
  (-> str
      (lgen.place-fixed :fire main-x main-y 10 10 2 2)
      (lgen.place-fixed :player main-x main-y 10 7 3 3)
      (lgen.place-fixed :dog main-x main-y 12 12 1 1)
      (lgen.place-random :tree main-x main-y 20 20 1 1)
      ;; (lgen.place-random :dog dog-x dog-y 20 20 1 1)
      ;; (lgen.place-random :revive revive-x revive-y 20 20 1 1)
      )
  (each [_key [x y] (pairs eggs)]
    ;; (lgen.place-random str :monster x y 20 20 1 1)
    (lgen.place-random str :egg x y 20 20 3 3)
    (lgen.place-random str :tree x y 20 20 3 3))
(each [_key [x y] (pairs dogs)]
    (lgen.place-random str :monster x y 20 20 1 1))
  (each [_key [x y] (pairs gems)]
    (lgen.place-random str :gem x y 20 20 3 3))
  (each [_key [x y] (pairs trees)]
    (lgen.place-random str :crushed-egg x y 20 20 4 4)
    (lgen.place-random str :tree x y 20 20 3 3))
  (local game-objects (lgen.to-flat-list str 20 20 30 30))
  game-objects)
