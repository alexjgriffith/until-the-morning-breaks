(local fire {})

(local loader (require :lib.loader))

(local state (require :state))

(fn fire.collide [fire])

(fn dir-to-level [value]
  (match value
    :high     :forward
    :medium   :right
    :low      :backward
    :very-low :left
    :out      :left))

(fn level-to-radius [level]
  (match level
    :high     50
    :medium   0
    :low      -25
    :very-low -50
    :out      -300))


(local gem-timer-red {:timer 0
                      :period 0.25
                      :rises [1 0 -1 -1 0 2 2 2]
                      :index 1})

(local gem-timer-blue {:timer 0.05
                       :period 0.25
                       :rises [1 0 -1 -1 0 2 2 2]
                       :index 6})

(local gem-timer-green {:timer 0.13
                        :period 0.25
                        :rises [1 0 -1 -1 0 2 2 2]
                        :index 3})

(fn update-timer [dt obj]
  (local {: period : rises} obj)
  (local next-time (+ dt obj.timer))
  (if (> next-time period)
      (do
        (tset obj :index (+ obj.index 1))
        (when (> obj.index (# rises))
          (tset obj :index 1))
        (tset obj :timer (- next-time period))
        )
      (tset obj :timer next-time)))

(fn get-rise [{: rises : index}]
  (. rises index))

(fn fire.add-log [self]
  (if (< self.time  (* 60 3))
    (do (tset self :time (+ (* 60 (math.ceil (/ self.time 60))) 60))
        true)
    false))

(fn fire.add-gem [self colour]
    (tset self colour true))

(fn fire.update [self dt]
  (state.lights.set-pos state.lights :fire self)
  (when (and self.blue self.red self.green)
    (love.event.push :game-over :gems)
    :game-over-win)
  (update-timer dt gem-timer-red)
  (update-timer dt gem-timer-blue)
  (update-timer dt gem-timer-green)
  (tset self :time (math.max 0 (- self.time (* (. (require :state) :burn-rate) dt))))
  (local previous self.level)
  (let [time self.time]
    (if (> time (* 3 60)) (tset self :level :high)
        (> time (* 2 60)) (tset self :level :medium)
        (> time (* 1 60)) (tset self :level :low)
        (> time (* 0 60)) (tset self :level :very-low)
        (tset self :level :out)))

  (match [previous self.level]
    [:very-low :out]
    (do (: (. _G.assets.sounds "fire-out") :setVolume 0.6)
        (: (. _G.assets.sounds "fire-out") :play)))
  (tset self :anim (.. (dir-to-level self.level) "-fire"))
  (self.sprite:update self.anim dt))

(fn fire.draw [self]
  (love.graphics.setColor 1 1 1 1)
  (if (~= :out self.level)
      (self.sprite:draw self.anim self.pos 1)
      (love.graphics.draw self.image self.quad
                          self.pos.x
                          (+ 8 self.pos.y)))
  (when self.red
    (love.graphics.draw self.image (. self.gem-quads :red)
                        (+ self.pos.x -2)
                        (- self.pos.y 3 (get-rise gem-timer-red))))
  (when self.blue
    (love.graphics.draw self.image (. self.gem-quads :blue)
                        (+ self.pos.x 5)
                        (- self.pos.y 7 (get-rise gem-timer-blue))))
  (when self.green
    (love.graphics.draw self.image (. self.gem-quads :green)
                        (+ self.pos.x 10)
                        (- self.pos.y 4 (get-rise gem-timer-green))))
  )

(local fire-mt {:__index fire
                :add-gem fire.add-gem
                :update fire.update
                :collide fire.collide
                :draw fire.draw})

(fn fire.strength [self]
  ;; (* (- (fire:strength) 1) 100)
  ;;(math.min 1 (/ self.time (* 4 60)))
  (level-to-radius self.level))

(fn fire.new [atlas x y]
  (local sprite (loader.load-four "assets/data/fire" "assets/sprites" 16 16 1 7))
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 256 256)))
  (local quad (new-quad :fire-out))
  (local gem-quads {:red (new-quad :red-gem)
                    :blue (new-quad :blue-gem)
                    :green (new-quad :green-gem)})
  (setmetatable {:pos {: x : y}
                 : sprite
                 :image atlas.image
                 :quad quad
                 :w 16 :h 16 :collision-type :slide
                 :col {:x 2 :y 8 :w 12 :h 8}
                 :time (* 60 4)
                 :type :fire
                 :name :fire
                 : gem-quads
                 :red false
                 :blue false
                 :green false
                 :level :high
                 :anim :forward-fire
                 :center {:x 8 :y 8}
                 :active true} fire-mt))

fire
