(local wood {})

(local loader (require :lib.loader))

(fn wood.collide [wood])

(fn wood.update [self dt])

(fn wood.draw [self]
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.draw self.image (. self.wood-quads self.count)
                      self.pos.x
                      self.pos.y))

(fn wood.increment [self]
  (if (< self.count 3)
      (do (tset self :count (+ self.count 1))
        true)
      false))

(fn wood.decrement [self]
  (if (> self.count 1)
      (do (tset self :count (- self.count 1))
          true)
      (= self.count 1)
      (do (tset self :count (- self.count 1))
          (remove-object-by-name self.name)
          true)
      false))

(local wood-mt {:__index wood
                :update wood.update
                :collide wood.collide
                :draw wood.draw})

(fn wood.new [atlas x y]
  (local wood-id (get-wood-id))
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 256 256)))
  (local wood-quads [(new-quad :logs-1) (new-quad :logs-2) (new-quad :logs-3)])
  (setmetatable {:pos {: x : y}
                 :image atlas.image
                 :count 1
                 : wood-quads
                 :w 16 :h 16 :collision-type :cross
                 :col {:x 2 :y 8 :w 12 :h 8}
                 :type :wood
                 :name (.. :wood- wood-id)
                 :active true} wood-mt))

wood
