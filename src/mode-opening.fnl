(local next-state {})

(var timer 0)

(local state (require :state))

(var step :leave)

(var alpha 1)

(var played false)

(local gamestate (require :lib.gamestate))

(local params (require :params))

(local mode-game (require :mode-game))

(local message-font ((. assets.fonts "avara") 40))

(local buttons (require :lib.buttons))

(local element-font
       {
        :text  ((. assets.fonts "avara") 40)
        :button  ((. assets.fonts "avara") 40)})

(local element-click
       {"Skip"
        (fn []
          (when (= :load step) (set timer 100)))})

(local element-hover {:button (fn [element x y] :nothing)})

(local elements [{:type :button :y 620  :oy -10 :ox -450 :w 300 :h 60 :text "Skip"}])

(local ui (buttons elements params element-click element-hover element-font))

(local menu-canvas (love.graphics.newCanvas 1280 720))
(fn next-state.draw [self]
  (mode-game.draw self)
  (when (~= 0 alpha)
    (love.graphics.setCanvas menu-background)
    (love.graphics.clear 1 1 1 (lume.clamp alpha 0 1))
    (love.graphics.setFont message-font)
    (local [R G B] params.colours.background)
    (love.graphics.setCanvas menu-canvas)
    (love.graphics.clear 0 0 0 0)
    (love.graphics.setColor R G B (lume.clamp alpha 0 1))
    (love.graphics.printf "Oh thou, cursed with eternal night!\n\nCollect the three Stones of Morning and Shepherd the animals to safety!\n\nBe warned, the Night darkens. Don't let it's minions take you.." (+ 0 200) (+ 0 200) 880 :center)
    (when (and (> timer 3) (= step :load))
      (ui:draw))
    )
  ;; (love.graphics.setColor 1 1 1 1)
  ;; (love.graphics.rectangle "line" 1 1 1278 718)
  (love.graphics.setCanvas)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.draw menu-background 0 0 0 2 2)
  (love.graphics.draw menu-canvas offsets.x offsets.y)
  (when (= step :done)
    (set played true)
    (gamestate.switch (require :mode-game) :continue)))


(fn next-state.update [self dt]
  ;; (mode-game.update self dt)
  (mode-game.update self dt)
  (ui:update dt)
  (match step
    :leave
    (do
      (if (> timer 1)
          (do
            (set step :load)
            (set timer 0)
            (set alpha 1)
            ;;(levels.next state)
            )
          (do
            (set timer (+ timer dt))
            (set alpha timer)))
      )
    :load (if (> timer 10)
              (do
                (love.mouse.setVisible false)
                (set step :enter)
                (set timer 0))
              (do
                (set timer (+ timer dt))))
    :enter
    (do
      (tset state :menu false)
      (if (< alpha 0)
          (do
            (set timer 0)
            (set step :done)
            (set alpha 0)
            )
          (do (set timer (+ timer dt))
              (set alpha (/ (- 2 timer) 2)))
      ))
    )
  )

(fn next-state.enter [self from ...]
  (if played
      (gamestate.switch (require :mode-game) :continue)
      (do
        ;; (love.mouse.setVisible false)
        (set step :leave)
        (tset state :menu true)
  )))

(fn next-state.leave [self from ...]
  (tset state :menu false)
  )

(fn next-state.mousereleased [self ...]
  (ui:mousereleased ...))

(fn next-state.keypressed [self key code]
  (match key
    :f10 (when (not state.web) (toggle-fullscreen))
    "m" (toggle-sound)
    "q" (when (not state.web) (screenshot))
    "esc" (= :load step)
    "tab" (= :load step)
    "return" (= :load step)
    _ (when (= :load step) (set timer 100))
    ))

next-state
