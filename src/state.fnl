(local state-consts {:dev false
                     :debug false
                     :web false})

(local default-state {:menu false
                      :difficulty :default
                      :description "Try your luck!"
                      :objects []
                      :wood-id 0
                      :world nil
                      :burn-rate 2
                      :detection 20
                      :hatch-rate 0.75
                      :top-line ""
                      :hunter "hunter-1"
                      :stall-time 0.5
                      :egg-respawn (* 60 3)
                      :debug false})

(local easy-state {:menu false
                   :difficulty :easy
                   :description "Easy, who said anything about easy!"
                   :objects []
                   :wood-id 0
                   :world nil
                   :burn-rate 1
                   :detection 20
                   :hatch-rate 0.5
                   :top-line ""
                   :hunter "hunter-2"
                   :stall-time 1
                   :egg-respawn (* 60 5)
                   :debug false})

(local hard-state {:menu false
                   :difficulty :hard
                   :description "Proceed at your own peril!"
                   :objects []
                   :wood-id 0
                   :world nil
                   :burn-rate 3
                   :detection 10
                   :hatch-rate 1.5
                   :top-line ""
                   :hunter "hunter-3"
                   :stall-time 0.2
                   :egg-respawn (* 60 1)
                   :debug false})

(local gg-state {:menu false
           :difficulty :gg
           :description "You'll need it! (SECRET MODE)"
           :objects []
           :wood-id 0
           :world nil
           :burn-rate 4
           :detection 10
           :hatch-rate 100
           :top-line ""
           :hunter "hunter-4"
           :stall-time 0
           :egg-respawn (* 60 1)
           :debug false})

(local sandbox-state {:menu false
                      :difficulty :sandbox
                      :description "Eliminate your chance of failure today!"
                      :objects []
                      :wood-id 0
                      :world nil
                      :burn-rate 0
                      :detection 20
                      :hatch-rate 1
                      :top-line ""
                      :hunter "hunter"
                      :stall-time 0.5
                      :egg-respawn 0
                      :debug false})

(global remove-object-by-name
        (fn [name]
          ;; HAX!! really should have all state handled in level :/
          (local state (require :state))
          (local objs state.objects)
          (var remove-id nil)
          (var remove-obj nil)
          (each [key obj (pairs objs)]
            (when (= obj.name name)
              (set remove-id key)
              (set remove-obj obj)))
          (if remove-id
              (do
                (local last (# objs))
                ;; (table.remove objs remove-id)
                (tset objs remove-id (. objs last))
                (table.remove objs last)
                (when (and remove-obj state.world (state.world:hasItem remove-obj))
                  (state.world:remove remove-obj)
                  true
                  )
                  true)
              false)))

(global get-object-by-name
        (fn [name]
          (local state (require :state))
          (local objs state.objects)
          (var found nil)
          (each [key obj (pairs state.objects)]
            (when (= obj.name name)
              (set found obj)))
          found))

(global get-wood-id
        (fn []
          (local state (require :state))
          (local wood-id state.wood-id)
          (tset state :wood-id (+ wood-id 1))
          wood-id))

(global reset-state
        (fn [difficulty?]
          (local state (require :state))
          (local new-state (lume.clone
                            (. {:default default-state
                                :easy easy-state
                                :hard hard-state
                                :sandbox sandbox-state
                                :gg gg-state}
                               (or difficulty? state.difficulty :default))))
          (local menu state.menu)
          (each [key value (pairs state)]
            (tset state key nil))
          (each [key value (pairs state-consts)]
            (tset state key value))
          ;; (for [i 1 (# state)]
          ;;   (tset state i nil))
          (each [key value (pairs new-state)]
            (tset state key (if (= "table" (type value))
                                (lume.clone value)
                                value)))
          (tset state :menu true)))

(global insert-object
        (fn [obj]
          (local state (require :state))
          (tset state.objects (+ 1 (# state.objects)) obj)
          (when (and state.world obj.col obj.pos obj.collision-type)
            (state.world:add obj
               (+ obj.col.x obj.pos.x)
               (+ obj.col.y obj.pos.y)
               obj.col.w
               obj.col.h obj.collision-type))))

(global get-fire
        (fn []
          (local state (require :state))
          (. state.objects 2)))

(global get-player
        (fn []
          (local state (require :state))
          (. state.objects 1)))

(global select-random-crushed-egg
        (fn []
          (local state (require :state))
          (local objs [])
          (each [i obj (pairs state.objects)]
            (when (and (not obj.respawning) (= obj.type :crushed-egg))
              (tset objs (+ 1 (# objs)) i)))
          (. state.objects (. objs (math.floor (lume.random 1 (+ 1 (# objs))))))))

;;{:menu false :objects [] :wood-id 0 :world nil :burn-rate 0 :hatch-rate 10 :top-line "" :hunter "hunter" :stall-time 0.2 :debug true}

(local st (lume.clone default-state))
(local co (lume.clone state-consts))
(local ret {})
(each [key value (pairs st)]
  (tset ret key value))

(each [key value (pairs co)]
  (tset ret key value))
ret
