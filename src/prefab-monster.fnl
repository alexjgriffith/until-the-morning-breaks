(local loader (require :lib.loader))
(local movement (require :lib.movement))
(local vec2d (require :vec2d))

(local monster {})

(local foots (require :footsteps))
(local emote (require :emote))

(local default-collide
       {:fire "slide"
        :chasm "slide"
        :tree "slide"
        :egg "slide"
        :dog "slide"
        :monster "cross"
        :wood "cross"
        :revive "cross"
})

(fn monster.collide [monster other]
  (. default-collide other.type))

(fn weighting [root pos min max]
  (lume.clamp (/ (- (lume.distance root.x root.y pos.x pos.y) min)
     (- max min)) 0 1))

(fn weighting-batch [root pos points weights min max]
  (vec2d.batch-shape root points weights {:weight (weighting root pos min max)
                                          :dpos pos}))

(fn steering [self]
  (local state (require :state))
  (local avoid-dist 20)
  (local search-dist 20)
  (local edge-dist 6)
  (local pos {:x (+ self.pos.x self.col.x 4)
              :y (+ self.pos.y self.col.y)})
  (tset self :dpos pos)
  (tset self :angle  (+ self.angle  (*  0.1 (- (love.math.noise
                                                (+ self.rv
                                                   (lume.random 0 0.1)
                                                   (* self.ni 0.001))) 0.5))))
  (tset self :ni (+ self.ni 1))
  (fn make-wander-goal [{: x : y} angle]
    (let [dx (math.cos self.angle)
          dy (math.sin self.angle)]
      {:x (+ x dx -4) :y (+ y dy)}))
  (local wander-goal {:pos (make-wander-goal pos self.angle) :col {:x 0 :y 0}})
  (local voids-objs-man (state.world:queryRect
                (- pos.x (* avoid-dist 8) (- 8))
                (- pos.y (* avoid-dist 8) (- 24 ) )
                (* avoid-dist 2 8)
                (* avoid-dist 2 8)
                (fn filter [item]
                  (match item.type
                    :tree true
                    :egg  true
                    :dog  true
                    :fire true
                    :player true
                    :monster true
                    :hunter true
                    ;; :chasm true
                    _ false))))

  (local voids-edge-man (state.world:queryRect
                         (- pos.x (* edge-dist 8))
                         (- pos.y (* edge-dist 8))
                         (* edge-dist 2 8)
                         (* edge-dist 2 8)
                         (fn filter [item]
                           (match item.type
                             :chasm true
                             _ false))))
  (local voids-man {})
  (when voids-objs-man
    (each [_ obj (ipairs voids-objs-man)]
    (table.insert voids-man obj)))
  (when voids-edge-man
    (each [_ obj (ipairs voids-edge-man)]
      (table.insert voids-man obj)))
  (local goals {})
  (local player (. state.objects 1))
  (local fire (. state.objects 2))
  (local player-distance (lume.distance self.pos.x self.pos.y player.pos.x player.pos.y))
  (local fire-distance (lume.distance self.pos.x self.pos.y fire.pos.x fire.pos.y))
  (if
   (or (< fire-distance (* 8 10))
       (and (not (= :chase self.state)) (< fire-distance (* 8 20))))
   (do
     (tset self :home fire.pos)
     (tset self :safe true)
     (tset goals 1 wander-goal)
     (tset self :state :at-fire)
     (tset self :max-speed (* 60 0.3)))

   (or (< player-distance (* 8 5)))
   (do (tset goals 1  wander-goal)
       (tset self :state :chase)
       (tset self :max-speed (* 60 0.1)))

   (or (< player-distance (* 8 10))
       (and  (= :chase self.state) (< player-distance (* 8 20))))
   (do (tset goals 1  player)
       (tset self :state :chase)
       (tset self :max-speed (* 60 1)))

   (do (tset goals 1 wander-goal)
       (tset self :state :wander)
       (tset self :max-speed (* 60 0.3))))


  (fn calc-weight [pa pb sq]
    (if (> (/ 1 (math.max 0.00001
                      (lume.distance
                       pa.x pa.y pb.x pb.y)))
             (/ 1 (* sq 8)))
      1
      0))

  (local void-objects (lume.map voids-man
                          (fn [obj]
                            (match obj.type
                              :fire (do (tset obj :dpos
                                              {:x (+ obj.pos.x obj.col.x 4)
                                               :y (+ obj.pos.y obj.col.y)})
                                        (tset obj :steering-weight
                                          (calc-weight pos obj.dpos 4)))
                              :monster (do (tset obj :dpos
                                              {:x (+ obj.pos.x obj.col.x 4)
                                               :y (+ obj.pos.y obj.col.y)})
                                        (tset obj :steering-weight
                                              (calc-weight pos obj.dpos 6)))
                              :player (do (tset obj :dpos
                                              {:x (+ obj.pos.x obj.col.x 4)
                                               :y (+ obj.pos.y obj.col.y)})
                                        (tset obj :steering-weight
                                              (calc-weight pos obj.dpos 2)))
                              :chasm (do
                                       (tset obj :dpos
                                              {:x (+ (* 8 obj.x) 4)
                                               :y (+ (* 8 obj.y) 4)})
                                        (tset obj :steering-weight
                                              (calc-weight pos obj.dpos 4))
                                        ;; (pp obj)
                                        )
                              _ (do (tset obj :dpos
                                          {:x (+ obj.pos.x obj.col.x 8)
                                           :y (+ obj.pos.y obj.col.y)})
                                    (tset obj :steering-weight
                                          (calc-weight pos obj.dpos 5))))
                            obj)))
  (local mask (vec2d.batch-dot-products pos self.points void-objects))
  (tset self :voids void-objects)
  (local goal-objects (lume.map goals
                           (fn [obj]
                             (match obj.type
                               _ (do (tset obj :steering-weight 1)
                                           (tset obj :dpos
                                                 {:x (+ obj.pos.x obj.col.x 4)
                                                  :y (+ obj.pos.y obj.col.y)})))
                             obj)))
  (tset self :goals goal-objects)
  (local chase-unweighted (vec2d.batch-dot-products pos self.points goal-objects))
  (local chase
         (if (= self.state :chase)
             chase-unweighted
             (weighting-batch pos self.home self.points chase-unweighted (* 8 5) (* 8 20))))
  (var chase-max 0.000001)
  (each [i c (ipairs chase)]
    (when (> c chase-max)
      (set chase-max c)))
  (each [i c (ipairs chase)]
    (tset chase i (/ c chase-max))
    (when (> (. mask i) 0)
      (tset chase i 0)))
  (local weights chase)
  (tset self :weights weights)
  (vec2d.batch-highest-weight self.points weights))

(fn moving [self dt]
  (local state (require :state))
  (local world state.world)
  (let [{:x dx :y dy} (steering self)
        x (+ self.pos.x (* self.max-speed dt dx))
        y (+ self.pos.y (* self.max-speed dt dy))]
    (local (ax ay cols len) (world:move self
                                        (+ x self.col.x)
                                        (+ y self.col.y)
                                        self.collide))
    ;; vestigle
    (if (or (~= ax (+ x self.col.x))
            (~= ay (+ y self.col.y)))
        (do (tset self.speed :x 0)
            (tset self.speed :y 0))
        (do (tset self.pos :x x)
          (tset self.pos :y y)))

    ))

(fn check-for-monsters [self]
  (local state (require :state))
  (local pos self.pos)
  (local check-dist (or state.detection 8))
  (local (hunters num) (state.world:queryRect
                (- pos.x (* check-dist 8) (- 8))
                (- pos.y (* check-dist 8) (- 8))
                (* check-dist 2 8)
                (* check-dist 2 8)
                (fn filter [item]
                  (match item.type
                    :hunter true
                    _ false))))
  (local previous self.sees-monster)
  (if (> num 0) (tset self :sees-monster true)
      (tset self :sees-monster false))
  (match [previous self.sees-monster]
    [false true] (self.emote:set :ex true)
    [true false] (self.emote:set :ex false)))

(fn check-for-player [self]
  (local player (get-player))
  (local previous self.sees-player)
  (tset self :sees-player
        (< (lume.distance (+ 8 player.pos.x) (+ 24 player.pos.y)
                          (+ 8 self.pos.x) (+ 8 self.pos.y))
           (* 12 8)))
  (match [previous self.sees-player]
    [false true] (self.emote:set :happy))
  self.sees-player)

(fn monster.update [self dt]
  (moving self dt)
  (when (check-for-player self)
    (check-for-monsters self))
  (self.footsteps:update {:x (+ 8 self.pos.x) :y (+ 8 self.pos.y)} self.angle)
  (self.emote:update dt self))

(fn monster.draw [self]
  (love.graphics.setColor 1 1 1 0.5)
  (love.graphics.draw self.image self.shadow-quad
                      self.pos.x
                      (+ self.pos.y (- self.h 14)))
  (love.graphics.setColor 1 1 1 1)
  (self.footsteps:draw)
  (love.graphics.draw self.image self.quad
                      self.pos.x
                      self.pos.y)
  (self.emote:draw))

(local monster-mt {:__index monster
                :update monster.update
                :collide monster.collide
                :draw monster.draw})

(fn monster.new [atlas x y creature level]
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 256 256)))
  (local footsteps (foots.new (+ x 8) (+ y 8) atlas.image
                              (new-quad :footstep-small-left)
                              (new-quad :footstep-small-right)
                              20
                              20))
  (local name (.. creature "-" level))
  (local shadow-quad (new-quad :shadow))
  (local quad (new-quad name))
  (local ex-quads [(new-quad :ex-1) (new-quad :ex-2)])
  (local happy-quads [(new-quad :happy-1) (new-quad :happy-2)])
  (setmetatable {:pos {:x x :y y}
                 :angle 0
                 : footsteps
                 :speed {:x 0 :y 0 :rate 20 :decay 40
                         :max 2 :max-s2 (/ 2 (math.sqrt 2)) :floor 0.2}
                 :image atlas.image
                 ;; : sprite
                 : shadow-quad
                 : quad
                 :emote (emote.new atlas -3 -13
                                   {: x : y}
                                   {:ex ex-quads :happy happy-quads})
                 : name
                 : level
                 : creature
                 :voids []
                 :goals []
                 :weights nil
                 :points (vec2d.batch-angle-to-point-unit)
                 :type :monster
                 :w (if (~= level "wolf") 16 16)
                 :h (if (~= level "wolf") 16 32)
                 :col (if (~= level "wolf")
                          {:x 4 :y 8 :w 8 :h 8}
                          {:x 4 :y 24 :w 8 :h 8})
                 :home {:x x :y y}
                 :ni 1
                 :ns 0.001
                 :na 1
                 :max-speed 0.3
                 :state :wander
                 :collision-type :cross
                 :rv (lume.random 0 10000)
                 :angle (lume.random 1 (* 2 math.pi))
                 :safe false
                 }
                monster-mt))

monster
