(local menu {})

(local gamestate (require :lib.gamestate))
(local buttons (require :lib.buttons))
(local params (require :params))
(local state (require :state))

(local mode-game (require :mode-game))

(var has-entered-new-game false)

(var ui nil)

(local element-font
       {:title  ((. assets.fonts "avara") 95)
        :subtitle  ((. assets.fonts "avara") 40)
        :text  ((. assets.fonts "avara") 20)
        :button  ((. assets.fonts "avara") 40)})

(local element-click
       {"Quit Game"
        (fn []
          (if (not state.web)
              (love.event.quit)
              (do (tset state :game-over :skip)
                  (gamestate.switch (require :mode-end-state)))
              ))

        "New Game"
        (fn []
          (when (_G.web-start-muted)
            (set-mute false))
          (web-start-unmute)
          (set has-entered-new-game true)
          (tset state :menu false)
          (assets.sounds.page:play)
          (gamestate.switch (require :mode-opening) :new-game)
          )

        "Continue"
        (fn []
          (tset state :menu false)
          (assets.sounds.page:play)
          ;;(gamestate.switch (require :mode-opening) :continue)
          (gamestate.switch (require "mode-game") :continue)
          )

        "Restart"
        (fn []
          (tset state :menu false)
          (assets.sounds.scratch:play)
          (gamestate.switch (require :mode-opening) :restart)
          ;;(gamestate.switch (require "mode-game") :restart)
          )

        "Controls"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-controls")))

        "Context"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-context")))

        "Difficulty"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-options")))

        "Mute"
        (fn []
          (toggle-sound))

        "Fullscreen"
        (fn []
          (if (not state.web)
              (toggle-fullscreen)
              (JS.callJS "toggleFullScreen();")))
          })

(local element-hover {:button (fn [element x y] :nothing)})

(fn menu.init [self]
  (when (not state.loaded)
      (tset state :loaded true)
      (tset state :menu false)
      (mode-game.enter self)
      (mode-game:update 0)
      ;; (when state.web (set-mute true))
      (tset state :menu true)
      ))

(fn menu.enter [self _ from]
  (love.mouse.setVisible true)
  (tset state :menu true)
  (local opening [{:type :title :y 130 :w 400 :h 60 :text "Until the Morning Breaks"}
                  {:type :subtitle :y 250 :w 400 :h 60 :text "( Kindred Game Jam Submission )"}
                  {:type :button :y 400 :oy -15 :ox 425 :w 350 :h 70 :text "New Game"}
                  {:type :button :y 500 :oy -15 :ox 425 :w 350 :h 70 :text "Controls"}
                  {:type :button :y 600 :oy -15 :ox 425 :w 350 :h 70 :text "Difficulty"}
                  {:type :button :y 400 :oy -15 :ox -425 :w 350 :h 70 :text "Mute"}
                  {:type :button :y 500 :oy -15 :ox -425 :w 350 :h 70 :text "Fullscreen"}
                  {:type :button :y 600 :oy -15 :ox -425 :w 350 :h 70 :text "Quit Game"}
                  ])
  (local standard [{:type :title :y 130 :w 400 :h 60 :text "Until the Morning Breaks"}
                  {:type :subtitle :y 250 :w 400 :h 60 :text "( Kindred Game Jam Submission )"}
                   {:type :button :y 400 :oy -15 :ox 250 :w 400 :h 70 :text "Continue"}
                   {:type :button :y 500 :oy -15 :ox 250 :w 400 :h 70 :text "Controls"}
                   {:type :button :y 600 :oy -15 :ox 250 :w 400 :h 70 :text "Difficulty"}

                   {:type :button :y 400 :oy -15 :ox -250 :w 400 :h 70 :text "Mute"}
                   {:type :button :y 500 :oy -15 :ox -250 :w 400 :h 70 :text "Fullscreen"}
                   {:type :button :y 600 :oy -15 :ox -250 :w 400 :h 70 :text "Quit Game"}
                   ])
  (local elements
         (if (and has-entered-new-game (~= from :end-game))
             standard
             opening))
  (set ui (buttons elements params element-click element-hover element-font)))

(local menu-canvas (love.graphics.newCanvas 1280 720))
(fn menu.draw [self]
  (love.graphics.clear 0 0 0 1)
  ;; (love.graphics.setCanvas draw-canvas)
  ;; (love.graphics.clear)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.push)
  ;; (local state (require :state))
  ;; (local player (. state.objects 1))
  ;; (love.graphics.scale camera.scale)
  ;; (love.graphics.translate (+ offsets.x (* camera.scale (+ 1 camera.x)))
  ;;                          (+ offsets.y (* camera.scale (+ 1 camera.y))))
  (mode-game.draw self)
  (love.graphics.pop)
  (love.graphics.setCanvas menu-background)
  (love.graphics.clear 1 1 1 0.3)
  (love.graphics.setCanvas menu-canvas)
  (love.graphics.clear 0 0 0 0)
  ;;(love.graphics.clear 1 1 1 0.3)
  ;;(love.graphics.setColor 1 1 1 0.1)
  (local (w h _flag) (love.window.getMode))
  ;; (love.graphics.rectangle "fill" 0 0 w h)
  (love.graphics.setColor 1 1 1 1)
  (ui:draw)
  (love.graphics.setFont element-font.text)
  (love.graphics.setCanvas)
  (love.graphics.setColor 1 1 1 1)
  ;; (love.graphics.clear params.colours.blue-grey)
  ;; (love.graphics.draw menu-canvas offsets.x offsets.y)
  (love.graphics.setCanvas)
  ;; (love.graphics.draw draw-canvas)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.draw menu-background 0 0 0 2 2)
  (love.graphics.draw menu-canvas offsets.x offsets.y)
  (love.graphics.setColor 0 0 0 1)
  (love.graphics.printf "By AlexJGriffith" (+ 880 offsets.x) (+ 680 offsets.y) 390 :right)
  (when (~= :default state.difficulty)
    (local difficulty-text
           (. {:default "Normal Mode"
               :hard "Hard Mode"
               :easy "Easy Mode"
               :gg "Good Luck"
               :sandbox "Sandbox Mode"}
              state.difficulty))
    (love.graphics.printf (.. difficulty-text ": " state.description) offsets.x (+ 60 offsets.y) 1280 :center)))

(fn menu.leave [self]
  (love.graphics.setCanvas))

(fn menu.update [self dt]
  (mode-game.update self dt)
  (local foot (. _G.assets.sounds "footsteps_snow"))
  (foot:setVolume 0)
  (ui:update dt))

(fn menu.mousereleased [self ...]
  (ui:mousereleased ...))

(fn menu.keypressed [self key code]
  (match key
    "m" (toggle-sound)
    "q" (when (not state.web) (screenshot))
    :f10 (when (not state.web)
           (do (toggle-fullscreen)))
    :escape (if has-entered-new-game
                ((. element-click "Continue"))
                ((. element-click "New Game")))
    :tab (if has-entered-new-game
                ((. element-click "Continue"))
                ((. element-click "New Game")))
    :return (if has-entered-new-game
                ((. element-click "Continue"))
                ((. element-click "New Game")))
    :space (if has-entered-new-game
                ((. element-click "Continue"))
                ((. element-click "New Game")))
    :. (do (tset state :game-over :skip)
           (gamestate.switch (require :mode-end-state)))
    ))

menu
