(local vec2d {})

(fn vec2d.batch-highest-weight [points weights]
  (var highest-weight 0)
  (var ret-point (. points 1))
  (each [i weight (ipairs weights)]
    (when (> weight highest-weight)
      (set ret-point (. points i))
      (set highest-weight weight)))
  ret-point)

(fn vec2d.batch-shape [root points weights object]
  "object should have weight, pos, weight-fun [norm-dot|dot]"
    (let [ret {}]
      (each [i point (ipairs points)]
        (let [weight-a (* (. weights i) (- 1 object.weight))
              weight-b (* (match object.weight-fun
                            :norm-dot
                            (let [[p1 p2] (vec2d.normals root object.dpos)]
                              (math.max (vec2d.dot-product root p1 object.dpos true)
                                        (vec2d.dot-product root p2 object.dpos true)))
                            _ (vec2d.dot-product root point object.dpos true))
                          object.weight)]
          (tset ret i (+ weight-a weight-b))))
      ret))

(fn vec2d.batch-dot-products [root points objects]
  (let [ret {}]
    (for [i 1 24] (tset ret i 0.5))
    (when objects
        (each [i point (ipairs points)]
          (var weight 0)
          (each [_ obj (ipairs objects)]
            (let [product (vec2d.dot-product root point obj.dpos true)]
              (set weight (+ weight (* product obj.steering-weight)))))
          (tset ret i weight)))
    ret))

(fn vec2d.batch-angle-to-point-unit []
  (let [step (/ (* 2 math.pi) 24)
        ret {}]
    (for [i 0 23]
      (tset ret (+ i 1) (vec2d.angle-to-point-unit (* step i))))
    ret))

(fn vec2d.angle-to-point-unit [angle]
  {:x (math.cos angle)
   :y (math.sin angle)})

(fn vec2d.angle-to-point [root angle r]
  {:x (+ root.x (* r (math.cos angle)))
   :y (+ root.x (* r (math.sin angle)))})

(fn vec2d.mag [pos]
  ;; so slow!
  (math.max 0.000001 (math.sqrt (^ pos.x 2) (^ pos.y 2))))

(fn vec2d.normalize [pos]
  (let [m (vec2d.mag pos)]
    {:x (/ pos.x m) :y (/ pos.y m)}))

(fn vec2d.dot-product [root pos1 pos2 normalize?]
  (let [{:x dx1 :y dy1}
        (if (not normalize?)
            (vec2d.normalize {:x (- pos1.x root.x ) :y (- pos1.y root.y )})
            pos1)
        {:x dx2 :y dy2}
        (vec2d.normalize {:x (- pos2.x root.x ) :y (- pos2.y root.y )})]
    (+ (* dx1 dx2) (* dy1 dy2))))


(fn vec2d.normals [root pos]
  (let [dx (- pos.x root.x)
        dy (- pos.x root.x)]
    [{:x dx :y (- dy)} {:x (- dx) :y dy}]))

vec2d
