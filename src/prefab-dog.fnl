(local dog {})

(local loader (require :lib.loader))

(local emote (require :emote))

(fn dog.collide [dog])

(local animations {:lie {:period 1.5
                         :rises [1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 3 3 3]}
                   :stand {:period 0.25
                           :rises [5 6]}
                   :standing {:period 0.25
                              :rises [1 4 5]
                              :repeat false
                              :next :stand}
                   :sitting {:period 0.25
                              :rises [5 4 1]
                              :repeat false
                              :next :lie}
                   })

(fn set-timer [s]
  (local anim (. animations s))
  {:timer 0
   :period anim.period
   :rises anim.rises
   :index 1
   :state s
   :next anim.next})

(var anim-timer (set-timer :lie))

(fn update-timer [dt obj]
  (local {: period : rises} obj)
  (local next-time (+ dt obj.timer))
  (if (> next-time period)
      (do
        (tset obj :index (+ obj.index 1))
        (when (> obj.index (# rises))
          (tset obj :index 1)
          (tset obj :state (or obj.next obj.state))
          (tset obj :rises (. animations (or obj.next obj.state) :rises))
          (tset obj :period (. animations (or obj.next obj.state) :period))
          (tset obj :next nil)
          )
        (tset obj :timer (- next-time period)))
      (tset obj :timer next-time)))

(fn get-rise [{: rises : index}] (. rises index))

(fn check-for-monsters [self]
  (local state (require :state))
  (local pos self.pos)
  (local check-dist 30)
  (local (hunters num) (state.world:queryRect
                (- pos.x (* check-dist 8) (- 8))
                (- pos.y (* check-dist 8) (- 8))
                (* check-dist 2 8)
                (* check-dist 2 8)
                (fn filter [item]
                  (match item.type
                    :hunter true
                    _ false))))
  (local previous self.sees-monster)
  (if (> num 0) (tset self :sees-monster true)
      (tset self :sees-monster false))
  (match [previous self.sees-monster]
    [false true] (self.emote:set :ex true)
    [true false] (self.emote:set :ex false)))

(fn dog.update [self dt]
  (local player (get-player))
  (check-for-monsters self)
  (if (or self.sees-monster
          (< (lume.distance player.pos.x player.pos.y self.pos.x self.pos.y) (* 3 8)))
      (tset self :state :stand)
      (tset self :state :lie))
  (match [self.state anim-timer.state]
    [:stand :lie] (do (set anim-timer (set-timer :standing))
                      (when (not self.sees-monster) (self.emote:set :happy)))
    [:lie :stand] (set anim-timer (set-timer :sitting)))
  (update-timer dt anim-timer)
  (self.emote:update dt self))

(fn dog.draw [self]
  (love.graphics.setColor 1 1 1 1)
  (when self.active
    (love.graphics.draw self.image self.shadow-quad
                        self.pos.x
                        (+ self.pos.y 1))
    (love.graphics.draw self.image (. self.dog-quads (get-rise anim-timer))
                      self.pos.x
                      self.pos.y)
    (self.emote:draw)))

(local dog-mt {:__index dog
                :update dog.update
                :collide dog.collide
                :draw dog.draw})

(fn dog.new [atlas x y]
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 256 256)))
  (local dog-quads [(new-quad :dog-sleep-1)
                    (new-quad :dog-sleep-2)
                    (new-quad :dog-eye)
                    (new-quad :dog-sit)
                    (new-quad :dog-stand-1)
                    (new-quad :dog-stand-3)
                    (new-quad :dog-stand-3)
                    ])
  (local shadow-quad (new-quad :shadow))
  (local ex-quads [(new-quad :ex-1) (new-quad :ex-2)])
  (local happy-quads [(new-quad :happy-1) (new-quad :happy-2)])
  (setmetatable {:pos {: x : y}
                 :image atlas.image
                 : dog-quads
                 :emote (emote.new atlas -3 -13
                                   {: x : y}
                                   {:ex ex-quads :happy happy-quads})
                 :sit-quad (new-quad :dog-sit)
                 : shadow-quad
                 :w 16 :h 16 :collision-type :cross
                 ;; :col {:x -4 :y 4 :w 16 :h 20}
                 :col {:x 4 :y 8 :w 8 :h 8}
                 :type :dog
                 :sees-monster false
                 :state :stand
                 :name :dog
                 :active true} dog-mt))

dog
