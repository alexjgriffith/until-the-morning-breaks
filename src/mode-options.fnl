(local options {})

(local gamestate (require :lib.gamestate))
(local buttons (require :lib.buttons))
(local params (require :params))

(local play-again (require :play-again))
(local mode-game (require :mode-game))

(local state (require :state))
(local assets _G.assets)

(local elements
       [{:type :title :y 40 :w 400 :h 60 :text "Until the Morning Breaks"}
        ;; {:type :subtitle :y 125 :w 400 :h 60 :text "( Kindred Game Jam Submission )"}
        {:type :subtitle :y 150 :w 400 :h 60 :text "Difficulty Levels"}
        {:type :button :y 250 :oy -13 :ox 0 :w 350 :h 70 :text "Normal Mode"}
        {:type :button :y 340 :oy -13 :ox 0 :w 350 :h 70 :text "Hard Mode"}
        {:type :button :y 430 :oy -13 :ox 0 :w 350 :h 70 :text "Easy Mode"}
        {:type :button :y 520 :oy -10 :ox 0 :w 350 :h 60 :text "Sandbox Mode"}
        {:type :button :y 610 :oy -13 :ox 0 :w 350 :h 70 :text "Back"}

        {:type :invisible-button :y 150 :oy -10 :ox -425 :w 350 :h 500 :text "Good Luck"}
        ])

(local element-font
       {:title  ((. assets.fonts "avara") 80)
        :subtitle  ((. assets.fonts "avara") 50)
        :button  ((. assets.fonts "avara") 40)
        :small-text-left  ((. assets.fonts "inconsolata") 30)})

(local element-click
       {"Normal Mode"
        play-again.normal
        "Easy Mode"
        play-again.easy
        "Hard Mode"
        play-again.hard
        "Sandbox Mode"
        play-again.sandbox
        "Good Luck"
        play-again.gg
        "Back"
        (fn []
          (assets.sounds.page:play)
          (gamestate.switch (require "mode-menu") :options))})

(local element-hover {:button (fn [element x y] :nothing)
                      :invisible-button (fn [element x y] :nothing)})

(local ui (buttons elements params element-click element-hover element-font))

(local menu-canvas (love.graphics.newCanvas 1280 720))
(fn options.draw [self]
  (love.graphics.setCanvas menu-canvas)
  (love.graphics.setColor params.colours.blue-grey)
  (love.graphics.rectangle "fill" 0 0 1280 720)
  (love.graphics.setColor 1 1 1 1)
  (ui:draw)
  (love.graphics.setCanvas)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.clear params.colours.blue-grey)
  (love.graphics.draw menu-canvas offsets.x offsets.y))

(fn options.update [self dt]
  (ui:update dt))

(fn options.mousereleased [self ...]
  (ui:mousereleased ...))

(fn options.keypressed [self key code]
  (match key
    :f10 (when (not state.web) (toggle-fullscreen))
    "escape"  (gamestate.switch (require :mode-menu))
    "return"  (gamestate.switch (require :mode-menu))
    "tab"  (gamestate.switch (require :mode-menu))
    "m" (toggle-sound)
    "q" (when (not state.web) (screenshot))
    ))

options
