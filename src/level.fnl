(local state (require :state))

(local map (require :lib.map))

(local player (require :prefab-player))
(local fire (require :prefab-fire))
(local tree (require :prefab-tree))
(local egg (require :prefab-egg))
(local revive (require :prefab-revive))
(local monster (require :prefab-monster))
(local gem (require :prefab-gem))
(local col (require :prefab-col))
(local dog (require :prefab-dog))

(local loader (require :lib.loader))

(fn [tile-sheet]
  (local [h w] [170 170])
  (local mapin (map.new h w 20 151 20 151))
  (local tileset-batch (love.graphics.newSpriteBatch
                        tile-sheet (* 200 200 4)))
  (local level
          {:tile-sheet tile-sheet
           :map mapin
           :tileset-batch tileset-batch
           :w w
           :h h})
  (map.update-tileset-batch tileset-batch mapin :ground)
  (tset state :level level)
  (local structure ((require :make-level)))
  (local atlas (loader.load-atlas "assets/data/sprite-data" "assets/sprites"))
  (var k 3)
  (var tree-index 1)
  (local egg-colours ["red" "blue" "green"])
  (var egg-index 1)
  (var gem-index 1)
  (local monsters [["pdog" "grey"] ["pen" "grey"] ["fdog" "grey"]
                   ["pdog" "blue"] ["pen" "blue"] ["fdog" "blue"]])
  (var monster-index 1)
  (each [_ {: name :x px :y py} (ipairs structure)]
    (local [x y] [(* 8 px) (* 8 py)])
          (match name
            :tree  (do (tset state.objects k (tree.new atlas x y tree-index))
                       (set tree-index (+ 1 tree-index))
                       (set k (+ k 1)))
            :player (tset state.objects 1 (player.new atlas x y))
            :fire (tset state.objects 2 (fire.new atlas x y))
            :egg (do
                   (tset state.objects k
                         (egg.new atlas x y (. egg-colours egg-index)))
                   (set egg-index (+ 1 egg-index))
                   (set k (+ k 1)))
            :crushed-egg (do
                           (tset state.objects k
                                 (egg.new atlas x y nil :crushed-egg))
                           (set k (+ k 1)))
            :gem (do
                   (tset state.objects k
                         (gem.new atlas x y (. egg-colours gem-index)))
                   (set gem-index (+ 1 gem-index))
                   (set k (+ k 1)))
            ;; :revive (tset state.objects k (revive.new atlas x y))
            :dog (do (tset state.objects k (dog.new atlas x y))
                     (set k (+ k 1)))
            :monster (do
                       (tset state.objects k
                             (monster.new atlas x y
                                          (. monsters monster-index 1)
                                          (. monsters monster-index 2)))
                       (set monster-index (+ 1 monster-index))
                       (set k (+ k 1)))
            )
    )
  ;; (tset state.objects (+ 1 (# state.objects)) (player.new atlas (* 8 70) (* 8 67)))
  ;; (tset state.objects (+ 1 (# state.objects)) (fire.new (* 8 70) (* 8 70)))
  ;; (tset state.objects (+ 1 (# state.objects)) (tree.new atlas (* 8 65) (* 8 65) 1))
  ;; (tset state.objects (+ 1 (# state.objects)) (egg.new atlas (* 8 48) (* 8 48) "red"))
  ;; (tset state.objects (+ 1 (# state.objects)) (revive.new atlas (* 8 40) (* 8 40) ))
  ;; (tset state.objects (+ 1 (# state.objects)) (monster.new atlas (* 8 40) (* 8 20) "pen" "wolf"))
  (tset state :world (col.new state.level.map state.objects))
  )
