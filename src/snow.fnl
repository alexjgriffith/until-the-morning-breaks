(local snow {})

(local loader (require :lib.loader))

(local state (require :state))

(fn snow.update [self dt]
  (self.part:update dt))

(fn snow.draw [self]
  (love.graphics.push "all")
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.setCanvas self.canvas)
  (love.graphics.clear 1 1 1 0)
  (love.graphics.draw self.part 0 0)
  (love.graphics.setCanvas)
  (love.graphics.pop)
  self.canvas)

(fn ff [self frames]
    (for [_i 1 frames]
      (self.part:update 0.0166)))

(local snow-mt {:__index snow
                :update snow.update
                :draw snow.draw})

(fn setup-part [snow-canvas]
  (local part (love.graphics.newParticleSystem snow-canvas (if state.web 300 2000)))

  (part:setParticleLifetime 2 5)
  (part:setEmissionRate 50000)
  (part:setSizeVariation 0.1)
  (part:setSpeed 100 200)
  (part:setSpin 0.1 1)
  (part:setSpread 0.25)
  ;;(part:setEmissionArea :uniform 1280 1280)
  (if state.web
      (part:setEmissionArea :uniform 250 250)
      (part:setEmissionArea :uniform 400 400))
  (part:setDirection 0.25)
  ;; (part:setLinearAcceleration -20 -20 20 20)
  (part:setRotation -1 1)
  (part:setColors 1 1 1 1 1 1 1 0)
  part
)

(fn snow.replace-part [self]
  (tset self :part (setup-part self.snow-canvas)))

(fn snow.new [x y s n]
  (local atlas (loader.load-atlas "assets/data/sprite-data" "assets/sprites"))
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 256 256)))
  (local snow-canvas (love.graphics.newCanvas 4 4))
  (local canvas (love.graphics.newCanvas 1280 1280))
  (local quad (new-quad :snow))
  (love.graphics.push "all")
  (love.graphics.setCanvas snow-canvas)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.draw atlas.image quad)
  (love.graphics.setCanvas)
  (love.graphics.pop)
  (local part (setup-part snow-canvas))
  (local ret (setmetatable {: x : y
                            : canvas
                            :image atlas.image
                            :quad quad
                            : s
                            : n
                            : part
                            : snow-canvas
                            :type :snow
                            :active true} snow-mt))
  (ff ret 200)
  ret)

snow
