(local lgen {})

(fn toxy [index]
  (lume.map (lume.split index ",") tonumber))

(fn toindex [i j]
  (.. i "," j))

(fn lgen.select-all [w h]
  (var selected {})
  (var k 1)
  (for [i 1 w]
    (for [j 1 h]
      (tset selected k [i j])
      (set k (+ k 1))))
  selected)

(fn lgen.select-square [x y v]
  (local selected [])
  (var k 1)
  (for [i (- v) v]
    (for [j (- v) v]
      (tset selected k [(+ x i) (+ y j)])
      (set k (+ k 1))
    ))
  selected)

(fn lgen.select-not-square [w h x y v]
  (local selected [])
  (var k 1)
  (for [i 1 w]
    (for [j 1 h]
      (when (or (or (> i (+ x v)) (< i (- x v)))
                 (or (> j (+ y v)) (< j (- y v))))
        (tset selected k [ i j])
        (set k (+ k 1)))))
  selected)

(fn lgen.choose-rand [str]
  (if (<= 1 (# str))
      [(. str (lume.random 1 (# str)))]
      false))

(fn lgen.choose-n [str n]
  (if (<= n (# str))
      (-> str (lume.shuffle) (lume.first n))
      false))

(fn lgen.subtract [str no]
  (var str2 {})
  (var temp {})
  (var k 1)
  (each [_ [x y] (pairs str)]
    (local key (toindex x y))
    (if (. temp key)
        (tset temp key (+ 1 (. temp key)))
        (tset temp key 1)))
  (each [_ [x y] (pairs no)]
    (local key (toindex x y))
    (if (. temp key)
        (tset temp key (+ 2 (. temp key)))
        (tset temp key 1)))
  (each [key value (pairs temp)]
    (local ret (toxy key))
    (when (= 1 value)
      (tset str2 k ret)
      (set k (+ k 1))))
  str2)

(fn lgen.union [str no]
  (var str2 {})
  (var temp {})
  (var k 1)
  (each [_ [x y] (pairs str)]
    (local key (toindex x y))
    (if (. temp key)
        (tset temp key (+ 1 (. temp key)))
        (tset temp key 1)))
  (each [_ [x y] (pairs no)]
    (local key (toindex x y))
    (if (. temp key)
        (tset temp key (+ 1 (. temp key)))
        (tset temp key 1)))
  (each [key value (pairs temp)]
    (local ret (toxy key))
    (when (< 0 value)
      (tset str2 k ret)
      (set k (+ k 1))))
  str2)

(fn lgen.intersect [str no]
  (var str2 {})
  (var temp {})
  (var k 1)
  (each [_ [x y] (pairs str)]
    (local key (toindex x y))
    (if (. temp key)
        (tset temp key (+ 1 (. temp key)))
        (tset temp key 1)))
  (each [_ [x y] (pairs no)]
    (local key (toindex x y))
    (if (. temp key)
        (tset temp key (+ 1 (. temp key)))
        (tset temp key 1)))
  (each [key value (pairs temp)]
    (local ret (toxy key))
    (when (= 2 value)
      (tset str2 k ret)
      (set k (+ k 1))))
  str2)

(fn lgen.not [str w h]
  (lgen.subtract (lgen.select-all w h) str))

(fn lgen.strip-blockers [str]
  (local str2 {})
  (each [key value (pairs str)]
    (tset str2 key {})
    (each [k op (pairs value)]
      (when (not (string.match op "-blocker$"))
        (tset (. str2 key) k op))))
  str2)

(fn lgen.to-flat-list [str0 bw bh ox oy]
  (local str (lgen.strip-blockers str0))
  (local str2 [])
  (var n 1)
  (each [key value (pairs str)]
    (local [i j] (toxy key))
    (each [k op (pairs value)]
      (local [ip jp] (toxy k))
      (tset str2 n {:name op
                    :x (+ ip ox (* bw (- i 1)))
                    :y (+ jp oy (* bh (- j 1)))})
      (set n (+ n 1))
      ))
  str2)

(fn lgen.place-fixed [str name i j x y w? h?]
  (let [w (or w? 1)
        h (or h? 1)
        tab (or (. str (toindex i j)) {})]
    (for [n 1 w ]
      (for [m 1 h]
        (tset tab (toindex (+ x n) (+ y m)) (.. name "-blocker"))
        (tset tab (toindex (- x n) (- y m)) (.. name "-blocker"))
        (tset tab (toindex (+ x n) (- y m)) (.. name "-blocker"))
        (tset tab (toindex (- x n) (+ y m)) (.. name "-blocker"))))
    (tset tab (toindex x y) (.. name))
    (tset str (toindex i j) tab))
  str)

(fn lgen.place-random [str name i j bw bh w? h?]
  (let [w (or w? 1)
        h (or h? 1)
        tab (or (. str (toindex i j)) {})]
    (var options {})
    (for [n w (- bw w)]
      (for [m h (- bh h)]
        (tset options (toindex n m) :valid)
        ))
    (each [key _value (pairs tab)]
      (let [[ip kp] (toxy key)]
       (for [n (- w) w]
            (for [m (- h) h]
              (tset options (toindex (+ n ip) (+ m kp)) nil)))))
    (var opts [])
    (var k 1)
    (each [key _value (pairs options)]
      (tset opts k key)
      (set k (+ k 1)))
    (match (- k 1)
      0 false
      _ (let [[px py] (toxy (. opts (math.ceil (lume.random 1 (- k 1)))))]
          (lgen.place-fixed str name i j px py w h)))))
lgen
