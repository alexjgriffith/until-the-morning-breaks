(local gem {})

(local loader (require :lib.loader))

(fn gem.collide [gem])

(var timer 0)
(local period 0.25)
(local rises [-9 -8 -7 -7 -9 -10 -10 -10])
(var index 1)

(fn gem.update [self dt]
  (local next-time (+ dt timer))
  (if (> next-time period)
      (do
        (set index (+ index 1))
        (when (> index (# rises))
          (set index 1))
        (set timer (- next-time period))
        )
      (set timer next-time)))

(fn gem.decrement [self]
  (remove-object-by-name self.name))

(fn gem.draw [self]
  (love.graphics.setColor 1 1 1 1)
  (when self.active
    (love.graphics.draw self.image self.shadow-quad
                        self.pos.x
                        (+ self.pos.y 0))
    (love.graphics.draw self.image self.gem-quad
                      self.pos.x
                      (+ 0 (. rises index) self.pos.y))))

(local gem-mt {:__index gem
                :update gem.update
                :collide gem.collide
                :draw gem.draw})

(fn gem.new [atlas x y colour]
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 256 256)))
  (local name (.. colour "-gem"))
  (local gem-quad (new-quad name))
  (local shadow-quad (new-quad :small-shadow))
  (setmetatable {:pos {: x : y}
                 :image atlas.image
                 : gem-quad
                 : shadow-quad
                 : name
                 : colour
                 :w 8 :h 8 :collision-type :cross
                 ;; :col {:x -4 :y 4 :w 16 :h 20}
                 :col {:x 0 :y 0 :w 8 :h 8}
                 :type :gem
                 :active true} gem-mt))

gem
