;; N The A
;; The A S
;; N T of the P

(local possible-elements {:n true :a true :s true :p true :t true})

(local assets _G.assets)

(local name-element-struct
       {:n :base-names
        :a :adjectifs
        :s :subjects
        :p :places
        :t :titles})

(local structures [[:n "the" :a]
                   [:n "the" :t]
                   [:n "the" :s]
                   [:n "the" :a :t]
                   [:n "the" :a :s]
                   ["The" :a :s]
                   ["The" :t "of" :p]
                   ])

(fn load-name-data []
  (local ret {})
  (each [key file (pairs name-element-struct)]
    (let [file (. _G.assets.text file)
          [shared bird wolf] (lume.split file "\n\n###\n\n")]
      (tset ret key {:bird (-> (lume.concat (lume.split bird "\n")
                                            (lume.split shared "\n"))
                               (lume.filter (fn [x] (~= x ""))))
                     :wolf (->
                            (lume.concat (lume.split wolf "\n")
                                         (lume.split shared "\n"))
                            (lume.filter (fn [x] (~= x "")))
                            )})))
  ret)

(local names (load-name-data))

(fn space-concat [string-1 string-2]
  (.. string-1 " " string-2))

(fn replace-elements [creature]
  (fn replace [str]
    (if (. possible-elements str)
          (lume.randomchoice (. names str creature))
          str)))

(fn test []
  (local n (require :names))
  (pp (n :wolf))
  (pp (n :bird))
  )

(fn sp [x]
  (pp x)
  x)

(fn generate-name [creature]
  (-> structures
      (lume.randomchoice)
      (lume.map (replace-elements creature))
      (lume.reduce space-concat)))
