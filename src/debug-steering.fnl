(fn draw [{:dpos {:x x :y y}  : points : weights : voids : goals}]
  (love.graphics.setColor 0 0 1 1)
  (when (and x y)
    (love.graphics.circle "fill" x y 20))
  (love.graphics.setColor 1 0 0 1)
  (each [_ {:dpos {:x vx :y vy} : type} (pairs voids)]
    (when (and vx vy (not (= :monster type)))
      (love.graphics.circle "fill"  vx vy  20)))
  (love.graphics.setColor 0 1 0 1)
  (each [_ {:dpos {:x gx :y gy}} (pairs goals)]
    (when (and gx gy)
      (love.graphics.circle "fill"  gx gy 20)))
  (when (and x y weights)
    (love.graphics.setColor 1 1 1 1)
    (each [i {:x dx :y dy} (ipairs points)]
      (local weight (math.max (. weights i) 0))
      (love.graphics.line (+ x (* 10 dx))
                          (+ y (* 10 dy))
                          (+ x (* 10 dx) (* 10 dx weight))
                          (+ y (* 10 dy) (* 10 dy weight)))))
  (love.graphics.setColor 1 1 1 1)
  )
