(local revive {})

(local loader (require :lib.loader))

(fn revive.collide [revive])

(var timer 0)
(local period 0.25)
(local rises [-9 -7 -8 -8 -7 -9 -10 -10 -10])
(var index 1)

(fn revive.update [self dt]
  (local next-time (+ dt timer))
  (if (> next-time period)
      (do
        (set index (+ index 1))
        (when (> index (# rises))
          (set index 1))
        (set timer (- next-time period))
        )
      (set timer next-time)))

(fn revive.draw [self]
  (love.graphics.setColor 1 1 1 1)
  (when self.active
    (love.graphics.draw self.image self.shadow-quad
                        self.pos.x
                        (+ self.pos.y 0))
    (love.graphics.draw self.image self.revive-quad
                      self.pos.x
                      (+ 0 (. rises index) self.pos.y))))

(local revive-mt {:__index revive
                :update revive.update
                :collide revive.collide
                :draw revive.draw})

(fn revive.new [atlas x y]
  (fn new-quad [member]
    (let [{: x : y : w : h} (. atlas.param.frames member :frame)]
      (love.graphics.newQuad  x y w h 256 256)))
  (local revive-quad (new-quad :potion))
  (local shadow-quad (new-quad :small-shadow))
  (setmetatable {:pos {: x : y}
                 :image atlas.image
                 : revive-quad
                 : shadow-quad
                 :w 8 :h 8 :collision-type :cross
                 ;; :col {:x -4 :y 4 :w 16 :h 20}
                 :col {:x 0 :y 0 :w 8 :h 8}
                 :type :revive
                 :name :revive
                 :active true} revive-mt))

revive
